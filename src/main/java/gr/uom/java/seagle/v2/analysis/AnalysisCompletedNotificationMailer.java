package gr.uom.java.seagle.v2.analysis;

import gr.uom.java.seagle.v2.SeagleConstants;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.se.util.config.ConfigManager;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Thodoris Chaikalis
 */
public class AnalysisCompletedNotificationMailer {

    private static Properties mailServerProperties;
    private final static java.util.logging.Logger logger = java.util.logging.Logger.getLogger(AnalysisCompletedNotificationMailer.class.getName());
    private static Session getMailSession;

    public static void sendNotificationEmailUsingGmail(SeagleManager seagleManager, String userEmail, Project project) throws NoSuchProviderException, MessagingException {
        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");
        getMailSession = Session.getDefaultInstance(mailServerProperties, null);
        Transport transport = getMailSession.getTransport("smtp");
        MimeMessage msg = generateMessage(getMailSession, userEmail, project);

	ConfigManager config = seagleManager.getManager(ConfigManager.class);
        String gmailAccountUsername = "seagle.uom";
        String gmailAccountPass = "YOU_SHOULD_CHANGE_THIS!"; 
        try{
            gmailAccountUsername = config.getProperty(SeagleConstants.SEAGLE_DOMAIN, "GMAIL_USERNAME", String.class);
            gmailAccountPass = config.getProperty(SeagleConstants.SEAGLE_DOMAIN, "GMAIL_USERNAME", String.class);
        }
        catch(Exception e){
            logger.warning("Login details for your Gmail account not found! Email notification system would propably not work!");
        }
     
        transport.connect("smtp.gmail.com", gmailAccountUsername, gmailAccountPass);
        transport.sendMessage(msg, msg.getAllRecipients());
        transport.close();
        sendInfoMessageToAdmin(userEmail, project);
    }

    public static void sendNotificationEmailWithLocalMailServer(String userEmail, int pid, Project project) {
        if (userEmail != null && !userEmail.isEmpty()) {
            Properties props = new Properties();
            props.setProperty("mail.smtp.auth", "true");
            props.put("mail.smtp.host", "localhost");

            javax.mail.Session mailSession = javax.mail.Session.getInstance(props, null);
            try {
                MimeMessage msg = generateMessage(mailSession, userEmail, project);
                Transport transport = mailSession.getTransport("smtp");
                transport.connect("localhost", 25, "username", "password");
                transport.sendMessage(msg, msg.getAllRecipients());
                transport.close();
            } catch (MessagingException mex) {
                logger.log(Level.SEVERE, "send of notification mail failed, exception: ", mex);
            }
        }
    }

    private static MimeMessage generateMessage(Session mailSession, String userEmail, Project project) throws MessagingException {
        String projectName = project.getName();
        int versions = project.getVersions().size();
        String pUrl = project.getRemoteRepoPath();

        MimeMessage msg = new MimeMessage(mailSession);
        msg.setFrom("seagle.uom@gmail.com");
        msg.setRecipients(Message.RecipientType.TO, userEmail);
        msg.setSubject(projectName + " Analysis Completed");
        msg.setSentDate(new Date());

        String messageText = "<html>"
                + "<body>"
                + "Dear researcher,</br>"
                + "</br></br>"
                + "The analysis for the project " + projectName + " has now been completed.</br>"
                + "<p>You may view your results in the link below:</p>"
                + "<a href=http://se.uom.gr/seagle/_/php/_startProjectSession.php?pname=" + projectName
                + "&githubpath=" + pUrl + ""
                + "&versions=" + versions + ">" + projectName + " report dashboard"
                + "</a>"
                + "</br>"
                + "<p>Thank you!</p>"
                + "<p>The SEAgle Team .</p>"
                + "</body>"
                + "</html>";
        msg.setContent(messageText, "text/html; charset=utf-8");
        return msg;
    }

    private static void sendInfoMessageToAdmin(String userEmail, Project project) throws NoSuchProviderException, MessagingException {
        if (!userEmail.contains("teohaik") || !userEmail.contains("chaikalis")) {
            String projectName = project.getName();
            
            Transport transport = getMailSession.getTransport("smtp");
            MimeMessage msg = new MimeMessage(getMailSession);
            msg.setFrom("seagle.uom@gmail.com");
            msg.setRecipients(Message.RecipientType.TO, "teohaik@gmail.com");
            msg.setSubject(projectName + " Analysis requested from other user completed");
            msg.setSentDate(new Date());
            
            String messageText = "<html>"
                + "<body>"
                + "Dear admin,</br>"
                + "</br></br>"
                + "The researcher with email " + userEmail + " just asked for the analysis of project "+projectName+"</br>"
                + "</br></br>"
                + "</body>"
                + "</html>";
            
            msg.setContent(messageText, "text/html; charset=utf-8");
	
            transport.connect("smtp.gmail.com", "seagle.uom", "28!dec@00");
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
        }
    }


    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }
}
