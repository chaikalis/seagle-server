package gr.uom.java.seagle.v2.analysis.metrics;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.analysis.project.evolution.JavaProject;
import gr.uom.java.seagle.v2.analysis.project.evolution.SourceCodeEvolutionAnalysis;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.controllers.NodeFacade;
import gr.uom.java.seagle.v2.db.persistence.util.PersistenceUtility;
import gr.uom.java.seagle.v2.metric.MetricRunInfo;
import gr.uom.java.seagle.v2.metric.imp.AbstractMetricTask;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.java.seagle.v2.scheduler.SchedulerConfig;
import gr.uom.java.seagle.v2.scheduler.SeagleTaskSchedulerManager;
import gr.uom.se.util.concurrent.TaskType;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
public class SourceCodeEvolutionAnalysisMetricTask extends AbstractMetricTask {

    private static final Logger logger = Logger.getLogger(SourceCodeEvolutionAnalysisMetricTask.class.getName());

    private final ProjectManager projectManager;
    
    private List<JavaProject> projects;

    public SourceCodeEvolutionAnalysisMetricTask(SeagleManager seagleManager, MetricRunInfo info, TaskType type, Set<ExecutableMetric> metrics) {
        super(seagleManager, info, getType(seagleManager), metrics);
        this.projectManager = seagleManager.resolveComponent(ProjectManager.class);
    }

    private Project getProject(MetricRunInfo info1) throws RuntimeException {
        Object request = info1.request();
        AnalysisRequestInfo analysisInfo = (AnalysisRequestInfo) request;
        Project p = projectManager.findByUrl(analysisInfo.getRemoteProjectUrl());
        if (p == null) {
            throw new RuntimeException("project is null for url " + analysisInfo.getRemoteProjectUrl());
        }
        return p;
    }

    private SourceCodeEvolutionAnalysis createAnalysisInstance() {
        NodeFacade nodeFacade = seagleManager.resolveComponent(NodeFacade.class);
        SourceCodeEvolutionAnalysis sourceEvolAnalysis = new SourceCodeEvolutionAnalysis(seagleManager, nodeFacade, metrics);
        return sourceEvolAnalysis;
    }

    @Override
    protected void compute() {
        SourceCodeEvolutionAnalysis sourceEvolAnalysis = createAnalysisInstance();
        projects = sourceEvolAnalysis.run(getProject(info));
        logger.log(Level.INFO, "Metrics  computation finished ");
    }

    @Override
    protected void updateDB() {
       logger.log(Level.INFO, "Persisting Metrics  to Database ");
       PersistenceUtility pu = new PersistenceUtility(seagleManager);
       Project project = getProject(info);
       project.setSoftwareProjects(projects);
       pu.persistMetrics(project);
       logger.log(Level.INFO, "Persisting Metrics Done!");
    }
    
    private static TaskType getType(SeagleManager seagleManager) {
        SeagleTaskSchedulerManager scheduler = seagleManager
                .getManager(SeagleTaskSchedulerManager.class);
        if (scheduler == null) {
            throw new RuntimeException("can not resolve task scheduler from seagle manager");
        }
        TaskType type = scheduler.getType(SchedulerConfig.TASKTYPE_SERIAL);
        if (type == null) {
            throw new RuntimeException("Task type was not found " + SchedulerConfig.TASKTYPE_SERIAL);
        }
        return type;
    }

}
