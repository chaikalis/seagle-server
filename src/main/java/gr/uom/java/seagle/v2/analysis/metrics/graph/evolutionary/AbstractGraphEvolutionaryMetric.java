
package gr.uom.java.seagle.v2.analysis.metrics.graph.evolutionary;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.graph.evolution.GraphEvolution;
import gr.uom.java.seagle.v2.analysis.metrics.AbstractAnalysisMetric;
import gr.uom.java.seagle.v2.metric.Category;
import gr.uom.java.seagle.v2.metric.Language;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;

/**
 *
 * @author Thodoris Chaikalis
 * created: 23/02/2016
 */
public abstract class AbstractGraphEvolutionaryMetric extends AbstractAnalysisMetric implements ExecutableMetric {

    public AbstractGraphEvolutionaryMetric(SeagleManager seagleManager){
        super(seagleManager);
    }
    
    public abstract void calculate(GraphEvolution graphEvolution);

    @Override
    public String getCategory() {
        return Category.GRAPH_METRICS;
    }

    @Override
    public String[] getLanguages() {
        return new String[]{Language.UNSPECIFIED};
    }
    
}
