package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.ProjectTimeline;
import gr.uom.se.util.context.ContextManager;
import gr.uom.se.util.module.ModuleContext;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionProvider;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 * The default project manager implementation.
 * <p>
 *
 * @author Theodore Chaikalis
 * @author Elvis Ligu
 */
@Stateless
public class ProjectManagerBean implements ProjectManager {

   /**
    * The seagle manager injected in order to get access to configs.
    * <p>
    */
   @EJB
   SeagleManager seagleManager;

   /**
    * {@inheritDoc}
    */
   @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
   @Override
   public void clone(String remoteRepoPath) {
      resolveRepoManager().clone(remoteRepoPath);
   }

   private RepositoryManager resolveRepoManager() {
      RepositoryManager manager = seagleManager
            .getManager(RepositoryManager.class);
      if (manager == null) {
         throw new RuntimeException(
               "RepositoryManager instance could not be resolved from seagle manager");
      }
      return manager;
   }

   private ProjectDBManager resolveDBManager() {
      ProjectDBManager manager = seagleManager
            .getManager(ProjectDBManager.class);
      if (manager == null) {
         throw new RuntimeException(
               "ProjectDBManager instance could not be resolved from seagle manager");
      }
      return manager;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getRepositoryFolder(String repoUrl) {
      return resolveRepoManager().getRepositoryFolder(repoUrl);
   }

   /**
    * {@inheritDoc}
    */
   @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
   @Override
   public void update(String remoteRepoPath) {
      resolveRepoManager().update(remoteRepoPath);
   }

   /**
    * {@inheritDoc}
    */
   @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
   @Override
   public void delete(String remoteRepoPath) {
      resolveRepoManager().delete(remoteRepoPath);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean exists(String remoteRepoPath) {
      return resolveRepoManager().exists(remoteRepoPath);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public VCSRepository getRepository(String remoteRepoPath) {
      return resolveRepoManager().getRepository(remoteRepoPath);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void checkout(String repoUrl, String commitID) {
      resolveRepoManager().checkout(repoUrl, commitID);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void checkout(VCSRepository repo, VCSCommit commit) {
      resolveRepoManager().checkout(repo, commit);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void checkout(VCSRepository repo, String cid) {
      resolveRepoManager().checkout(repo, cid);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getCheckoutFolder(String repoUrl, String cid) {
      return resolveRepoManager().getCheckoutFolder(repoUrl, cid);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getCheckoutFolder(VCSRepository repo, String cid) {
      return getCheckoutFolder(repo.getRemotePath(), cid);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getCheckoutFolder(VCSRepository repo, VCSCommit commit) {
      return getCheckoutFolder(repo, commit.getID());
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String resolveProjectName(String projectUrl) {
      return resolveRepoManager().resolveProjectName(projectUrl);
   }

   @Override
   public List<ProjectTimeline> getTimelinesByProjectURLAndType(String url,
         String actionType) {
      return resolveDBManager()
            .getTimelinesByProjectURLAndType(url, actionType);
   }

   @Override
   public List<ProjectTimeline> getTimelinesByProjectNameAndType(String name,
         String actionType) {
      return resolveDBManager().getTimelinesByProjectNameAndType(name,
            actionType);
   }

   @Override
   public List<ProjectTimeline> getTimelinesByProjectURL(String url) {
      return resolveDBManager().getTimelinesByProjectURL(url);
   }

   @Override
   public List<ProjectTimeline> getTimelinesByProjectName(String name) {
      return resolveDBManager().getTimelinesByProjectName(name);
   }

   @Override
   public List<ProjectTimeline> getTimelinesByType(String type) {
      return resolveDBManager().getTimelinesByType(type);
   }

   @Override
   public List<ProjectTimeline> getTimelines(Date date) {
      return resolveDBManager().getTimelines(date);
   }

   @Override
   public List<ProjectTimeline> getTimelines(Date start, Date end) {
      return resolveDBManager().getTimelines(start, end);
   }

   @Override
   public List<ProjectTimeline> getTimelinesBefore(Date date) {
      return resolveDBManager().getTimelinesBefore(date);
   }

   @Override
   public List<ProjectTimeline> getTimelinesAfter(Date date) {
      return resolveDBManager().getTimelinesAfter(date);
   }

   @Override
   public void createTimeline(Project project, String actionType, Date date) {
      resolveDBManager().createTimeline(project, actionType, date);
   }

   @Override
   public void createTimelineForProjectURL(String url, String actionType,
         Date date) {
      resolveDBManager().createTimelineForProjectURL(url, actionType, date);
   }

   @Override
   public void createTimelineForProjectName(String name, String actionType,
         Date date) {
      resolveDBManager().createTimelineForProjectName(name, actionType, date);
   }

   @Override
   public ProjectTimeline getLastTimeline(String type) {
      return resolveDBManager().getLastTimeline(type);
   }

   @Override
   public ProjectTimeline getLastTimelineByProjectURL(String url, String type) {
      return resolveDBManager().getLastTimelineByProjectURL(url, type);
   }

   @Override
   public ProjectTimeline getLastTimelineByProjectName(String name, String type) {
      return resolveDBManager().getLastTimelineByProjectName(name, type);
   }

   @Override
   public void create(Project entity) {
      resolveDBManager().create(entity);
   }

   @Override
   public void edit(Project entity) {
      resolveDBManager().edit(entity);
   }

   @Override
   public Project findByName(String name) {
      return resolveDBManager().findByName(name);
   }

   @Override
   public void remove(Project entity) {
      resolveDBManager().remove(entity);
   }

   @Override
   public Project findByUrl(String purl) {
      return resolveDBManager().findByUrl(purl);
   }

   @Override
   public Project findById(Object id) {
      return resolveDBManager().findById(id);
   }

   @Override
   public List<Project> getAll() {
      return resolveDBManager().getAll();
   }

   @Override
   public List<Project> findCreatedAfter(Date date) {
      return resolveDBManager().findCreatedAfter(date);
   }

   @Override
   public List<Project> findCreatedBefore(Date date) {
      return resolveDBManager().findCreatedBefore(date);
   }

   @Override
   public int count() {
      return resolveDBManager().count();
   }

   @Override
   public List<Project> findUpdatedAfter(Date date) {
      return resolveDBManager().findUpdatedAfter(date);
   }

   @Override
   public List<Project> findUpdatedBefore(Date date) {
      return resolveDBManager().findUpdatedBefore(date);
   }

   @Override
   public boolean contains(Project entity) {
      return resolveDBManager().contains(entity);
   }

   @Override
   public List<Project> findInsertedAfter(Date date) {
      return resolveDBManager().findInsertedAfter(date);
   }

   @Override
   public List<Project> findInsertedBefore(Date date) {
      return resolveDBManager().findInsertedBefore(date);
   }

   @Override
   public List<Project> findCreatedWithin(Date start, Date end) {
      return resolveDBManager().findCreatedWithin(start, end);
   }

   @Override
   public List<Project> findUpdatedWithin(Date start, Date end) {
      return resolveDBManager().findUpdatedWithin(start, end);
   }

   @Override
   public List<Project> findInsertedWithin(Date start, Date end) {
      return resolveDBManager().findInsertedWithin(start, end);
   }

    @Override
    public ConnectedVersionProvider getVersions(String projectRepoURL) {
     ContextManager cm = seagleManager.getManager(ContextManager.class);
      try (VCSRepository repo = getRepository(projectRepoURL)) {
         ModuleContext context = cm.lookupContext(repo, ModuleContext.class);
         ConnectedVersionProvider versionProvider = context.load(ConnectedVersionProvider.class);
         if(versionProvider != null){
             return versionProvider;
         }
      }   
      return null;
    }

    @Override
    public void flushChanges() {
        resolveDBManager().flushChanges();
    }
}
