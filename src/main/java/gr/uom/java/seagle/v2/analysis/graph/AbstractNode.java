
package gr.uom.java.seagle.v2.analysis.graph;

import gr.uom.java.seagle.v2.db.persistence.Node;
import java.util.Objects;

/**
 *
 * @author Theodore Chaikalis
 */
public abstract class AbstractNode {

    private final String name;
    private int age;
    private String sourceFilePath;
    private Node dbNode;

    public AbstractNode(String name) {
        this.name = name;
        age = 0;
    }

    public void setSourceFilePath(String sourceFilePath) {
        this.sourceFilePath = sourceFilePath;
    }

    public String getSourceFilePath() {
        return sourceFilePath;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
    
    public void increaseAge(int previousAge){
        age = previousAge + 1;
    }

    public Node getDbNode() {
        return dbNode;
    }

    public void setDbNode(Node dbNode) {
        this.dbNode = dbNode;
    }
    
    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        int h = name.hashCode();
        int hash = (h >>> 20) ^ (h >>> 12);
        return hash ^ (hash >>> 7) ^ (hash >>> 4);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractNode other = (AbstractNode) obj;
        return Objects.equals(this.name, other.name);
    }

    
}
