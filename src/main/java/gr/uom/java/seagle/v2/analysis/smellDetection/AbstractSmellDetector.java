package gr.uom.java.seagle.v2.analysis.smellDetection;

import gr.uom.java.seagle.v2.AbstractSeagleResolver;
import gr.uom.java.seagle.v2.SeagleConstants;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import gr.uom.java.seagle.v2.db.persistence.MetricValue;
import gr.uom.java.seagle.v2.db.persistence.Node;
import gr.uom.java.seagle.v2.db.persistence.NodeVersionMetric;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.db.persistence.controllers.NodeFacade;
import gr.uom.java.seagle.v2.utilities.SeagleDataStructures;
import gr.uom.se.util.config.ConfigManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thodoris Chaikalis
 */
public abstract class AbstractSmellDetector extends AbstractSeagleResolver {

    protected double few;
    protected double many;
    protected double several;
    protected double featureEnvyLAALevel; //books say should be 0.3333
    protected double godClassATFD_TOP_VALUES_PERCENT;
    protected double godClassATFD_LOWER_LIMIT;
    protected double godClassTCC_HIGHER_LIMIT;
    protected double godClassWMC_LOWER_LIMIT;

    
    protected NodeFacade nodeFacade;
    protected Collection<Node> nodesToUpdate;

    private static final Logger logger = Logger.getLogger(AbstractSmellDetector.class.getName());

    public AbstractSmellDetector(SeagleManager seagleManager, String mnemonic) {
        super(seagleManager);
        ConfigManager config = seagleManager.getManager(ConfigManager.class);
        this.nodeFacade = super.resolveComponentOrManager(NodeFacade.class);
        try {
            few = Double.parseDouble(config.getProperty(SeagleConstants.SEAGLE_DOMAIN, SeagleConstants.SMELL_DETECTOR_FEW_VALUE, String.class));
            many = Double.parseDouble(config.getProperty(SeagleConstants.SEAGLE_DOMAIN, SeagleConstants.SMELL_DETECTOR_MANY_VALUE, String.class));
            several = Double.parseDouble(config.getProperty(SeagleConstants.SEAGLE_DOMAIN, SeagleConstants.SMELL_DETECTOR_SEVERAL_VALUE, String.class));
            featureEnvyLAALevel = Double.parseDouble(config.getProperty(SeagleConstants.SEAGLE_DOMAIN, SeagleConstants.SMELL_DETECTOR_FE_LAA_LIMIT, String.class));
            godClassATFD_TOP_VALUES_PERCENT = 
                    Double.parseDouble(config.getProperty(SeagleConstants.SEAGLE_DOMAIN, "godClassATFD_TOP_VALUES_PERCENT", String.class));
            godClassATFD_LOWER_LIMIT = 
                    Double.parseDouble(config.getProperty(SeagleConstants.SEAGLE_DOMAIN, "godClassATFD_LOWER_LIMIT", String.class));
            godClassTCC_HIGHER_LIMIT = 
                    Double.parseDouble(config.getProperty(SeagleConstants.SEAGLE_DOMAIN, "godClassTCC_HIGHER_LIMIT", String.class));
            godClassWMC_LOWER_LIMIT = 
                    Double.parseDouble(config.getProperty(SeagleConstants.SEAGLE_DOMAIN, "godClassWMC_LOWER_LIMIT", String.class));   
 
        } catch (Exception e) {
            logger.warning("Seagle_domain.properties file not found. Loading default metric threshold values");
            few = 2;
            many = 3;
            several = 4;
            featureEnvyLAALevel = 0.333;
            godClassATFD_TOP_VALUES_PERCENT = 0.2;
            godClassATFD_LOWER_LIMIT = 4;
            godClassTCC_HIGHER_LIMIT = 0.333;
            godClassWMC_LOWER_LIMIT = 20;
        }
        logger.log(Level.INFO, mnemonic + " initiated.\n Thresholds \n"
                + "FEW = {0}" + "\n"
                + "MANY = {1}" + "\n"
                + "SEVERAL = {2}" + "\n" 
                + "LAA limit for FE detection = {3}"
                + "ATFD Top Percent Limit for God Class detection = {4}"
                + "ATFD lowe limit for God Class detection = {5}"
                + "TCC higher limit for God Class detection = {6}"
                + "WMC lower limit for God Class detection = {7} ", 
                new Object[]{few, many, several, featureEnvyLAALevel,
                    godClassATFD_TOP_VALUES_PERCENT,
                    godClassATFD_LOWER_LIMIT,
                    godClassTCC_HIGHER_LIMIT,
                    godClassWMC_LOWER_LIMIT});

    }
    
    public abstract void detectSmells(Project project);
    
    public void detectProjectSmells(Collection<Node> nodesToUpdate, Project project){
        this.nodesToUpdate = nodesToUpdate;
        detectSmells(project);
    }

    protected Map<String, Double> getMetricValuesPerClass(Version version, String mnemonic) {
        Map<String, Double> valuePerClass = getMetricsFromMemory(version, mnemonic);
        if (valuePerClass.isEmpty()) {
            List<Node> nodes = nodeFacade.findByVersion(version);
            for (Node n : nodes) {
                Collection<NodeVersionMetric> computedMetrics = n.getComputedMetrics();
                getMetricsFromDB(computedMetrics, mnemonic, valuePerClass, n);
            }
        }
        return valuePerClass;
    }

    private Map<String, Double> getMetricsFromMemory(Version version, String mnemonic) {
        //Get metrics from memory
        Project project = version.getProject();
        List<? extends SoftwareProject> softwareProjects = project.getSoftwareProjects();
        for (SoftwareProject softProject : softwareProjects) {
            if (softProject.getProjectVersion().equals(version)) {
                Map<String, Map<String, Double>> metricValuesNodeLevel = softProject.getMetricValuesNodeLevel();
                return metricValuesNodeLevel.get(mnemonic);
            }
        }
        return new HashMap<>();
    }

    private void getMetricsFromDB(Collection<NodeVersionMetric> computedMetrics, String mnemonic, Map<String, Double> valuePerClass, Node n) {
        for (NodeVersionMetric nvm : computedMetrics) {
            MetricValue metricValue = nvm.getMetricValue();

            if (metricValue.getMetric().getMnemonic().equals(mnemonic)) {
                valuePerClass.put(n.getName(), metricValue.getValue());
                break;
            }
        }
    }

    public List<String> getClassesThatAreInTopPercentOfMetric(Map<String, Double> values, String mnemonic, double percentage) {
        List<String> classesInTopPercent = new ArrayList<>();

        //Map is sorted in Ascending order
        Map<String, Double> sortedMapByValue = SeagleDataStructures.sortMapByValue(values);

        int i = 0;
        int size = sortedMapByValue.keySet().size();
        int limitIndex = (int) (size * (1 - percentage));

        for (String key : sortedMapByValue.keySet()) {
            if (i >= limitIndex) {
                classesInTopPercent.add(key);
            }
            i++;
        }
        return classesInTopPercent;
    }

    public double getAverageValueOfMetric(Map<String, Double> values, String mnemonic) {
        double average = 0;
        int size = values.keySet().size();
        for (String key : values.keySet()) {
            Double value = values.get(key);
            average += value;
        }
        return (average / size);
    }

    public double getStandardDeviationOfMetric(Map<String, Double> values, double average, String mnemonic) {
        double variance = 0;
        double sumOfSquares = 0;
        int size = values.keySet().size();
        for (String key : values.keySet()) {
            Double value = values.get(key);
            sumOfSquares += Math.pow(value - average, 2);
        }
        variance = sumOfSquares / size;
        return Math.sqrt(variance);
    }

    public double getHIGHlimitOfMetric(Map<String, Double> values, String mnemonic) {
        double average = getAverageValueOfMetric(values, mnemonic);
        double std = getStandardDeviationOfMetric(values, average, mnemonic);
        return average + std;
    }

    public double getLOWlimitOfMetric(Map<String, Double> values, String mnemonic) {
        double average = getAverageValueOfMetric(values, mnemonic);
        double std = getStandardDeviationOfMetric(values, average, mnemonic);
        return average - std;
    }

    public double getVERY_HIGHlimitOfMetric(Map<String, Double> values, String mnemonic) {
        return getHIGHlimitOfMetric(values, mnemonic) * 1.5;
    }

}
