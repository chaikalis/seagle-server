package gr.uom.java.seagle.v2;

import gr.uom.se.util.module.annotations.Property;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * An instance that should be created by seagle manager containing info about
 * different seagle paths.
 * <p>
 * 
 * @author Theodore Chaikalis
 */
@Property(domain = SeagleConstants.SEAGLE_DOMAIN, name = SeaglePathConfig.PROPERTY)
public class SeaglePathConfig {

   /**
    * The name of the property were this instance is stored within config
    * manager, under the seagle domain.
    * <p>
    */
   public static final String PROPERTY = "seaglePathConfigInstance";

   /**
    * The seagle home folder.
    * <p>
    */
   @Property(domain = SeagleConstants.SEAGLE_DOMAIN, name = SeagleConstants.SEAGLE_HOME_PROPERTY)
   private String seagleHome;

   /**
    * The data home folder, relative to seagle home.
    * <p>
    */
   @Property(domain = SeagleConstants.SEAGLE_DOMAIN, name = SeagleConstants.DATA_HOME_PROPERTY)
   private String dataHome;

   /**
    * The projects home folder, relative to data home.
    * <p>
    */
   @Property(domain = SeagleConstants.SEAGLE_DOMAIN, name = SeagleConstants.PROJECTS_HOME_PROPERTY)
   private String projectsHome;

   /**
    * The repositories home folder, relative to projects home.
    * <p>
    */
   @Property(domain = SeagleConstants.SEAGLE_DOMAIN, name = SeagleConstants.REPOS_HOME_PROPERTY)
   private String reposHome;

   /**
    * The projects sources home folder, relative to projects home.
    * <p>
    */
   @Property(domain = SeagleConstants.SEAGLE_DOMAIN, name = SeagleConstants.PROJECTS_SOURCE_CODE_HOME_PROPERTY)
   private String projectsSourceCodeHome;

   /**
    * Used to create an instance in case a module loader is used.
    * <p>
    */
   public SeaglePathConfig() {
   }

   /**
    * Get the home folder of the seagle.
    * <p>
    * 
    * @return the home folder of the seagle
    */
   public Path getSeagleHome() {
      if (seagleHome == null) {
         seagleHome = SeagleConstants.SEAGLE_HOME;
      }
      return Paths.get(seagleHome);
   }

   /**
    * Get the data folder of the seagle.
    * <p>
    * This is where data should be stored in disk. The data home is under the
    * seagle home. Although this path is resolved to reflect that.
    *
    * @return the data folder of the seagle
    */
   public Path getDataPath() {
      if (dataHome == null) {
         dataHome = SeagleConstants.DATA_HOME;
      }
      Path seagle = getSeagleHome();
      return seagle.resolve(dataHome);
   }

   /**
    * Get the projects folder of the seagle.
    * <p>
    * This is where data related to cloned projects should be stored in disk.
    * The projects home is under the data home. Although this path is resolved
    * to reflect that.
    *
    * @return the cloned projects folder of the seagle
    */
   public Path getProjectsPath() {
      if (projectsHome == null) {
         projectsHome = SeagleConstants.PROJECTS_HOME;
      }
      Path data = getDataPath();
      return data.resolve(projectsHome);
   }

   /**
    * Get the repositories folder of the seagle.
    * <p>
    * This is where cloned repositories should be stored in disk. The
    * repositories home is under the projects home. Although this path is
    * resolved to reflect that.
    *
    * @return the data folder of the seagle
    */
   public Path getRepositoriesPath() {
      if (reposHome == null) {
         reposHome = SeagleConstants.REPOS_HOME;
      }
      Path projects = getProjectsPath();
      return projects.resolve(reposHome);
   }

   /**
    * Get the projects sources folder of the seagle.
    * <p>
    * This is where checked out repositories should be stored in disk. The
    * sources home is under the projects home. Although this path is resolved to
    * reflect that.
    *
    * @return the data folder of the seagle
    */
   public Path getProjectSourceCodePath() {
      if (projectsSourceCodeHome == null) {
         projectsSourceCodeHome = SeagleConstants.PROJECTS_SOURCE_CODE_HOME;
      }
      Path projects = getProjectsPath();
      return projects.resolve(projectsSourceCodeHome);
   }
}
