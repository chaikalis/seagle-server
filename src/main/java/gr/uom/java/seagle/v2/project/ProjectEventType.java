package gr.uom.java.seagle.v2.project;

import gr.uom.se.util.event.DefaultEvent;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventType;

/**
 *
 * @author Elvis Ligu & Theodore Chaikalis
 */
public enum ProjectEventType implements EventType {

   /**
    * Event type sent by ProjectManager to inform listeners about the clone of a
    * project.
    */
   CLONE,
   /**
    * Event type sent by ProjectManager to inform listeners about the update of
    * a project.
    */
   UPDATE,
   /**
    * Event type sent by ProjectManager to inform listeners about the remove of
    * a project.
    */
   REMOVE,
   /**
    * Event type sent by DBProjectUpdater to inform listeners about the
    * insertion of a new cloned project into DB.
    */
   DB_INSERTED,
   /**
    * Event type sent by DBProjectUpdater to inform listeners about the
    * update of an updated project into DB.
    */
   DB_UPDATED,
   /**
    * Event type sent by DBProjectUpdater to inform listeners about the
    * deletion of project from DB.
    */
   DB_REMOVED;

   public Event newEvent(EventInfo info) {
      return new DefaultEvent(this, info);
   }
}
