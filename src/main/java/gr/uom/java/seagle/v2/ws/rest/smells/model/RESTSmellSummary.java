/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.ws.rest.smells.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Thodoris
 */
@XmlRootElement(name = "smellSummary")
public class RESTSmellSummary extends RESTSmell{
    
    private int smellFrequency;
    private String smellName;

    public RESTSmellSummary(int smellFrequency, String smellName) {
        this.smellFrequency = smellFrequency;
        this.smellName = smellName;
    }

    public RESTSmellSummary() {
        smellFrequency = 0;
    }

    public int getSmellFrequency() {
        return smellFrequency;
    }

    public String getSmellName() {
        return smellName;
    }

    public void setSmellFrequency(int smellFrequency) {
        this.smellFrequency = smellFrequency;
    }

    public void setSmellName(String smellName) {
        this.smellName = smellName;
    }
    
    

}
