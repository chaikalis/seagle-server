package gr.uom.java.seagle.v2.analysis.graph.evolution;

import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectMetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.PropertyFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.VersionFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.metric.MetricManager;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.java.seagle.v2.scheduler.TaskSchedulerManager;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;
import javax.ejb.EJB;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;

/**
 *
 * @author Theodore Chaikalis
 */
public abstract class AbstractGraphMetricTest extends AbstractSeagleTest {

    public static final String demoUrl
            = "https://github.com/teohaik/evolutionTest.git";
    
    private static final Logger logger = Logger
           .getLogger(AbstractGraphMetricTest.class.getName());

    @EJB
    ProjectManager projectManager;

    @EJB
    MetricManager metricManager;

    @EJB
    EventManager eventManager;

    @EJB
    PropertyFacade properties;

    @EJB
    SeagleManager seagleManager;

    @EJB
    ProjectMetricFacade projectMetricFacade;
    
    @EJB
    VersionFacade versionFacade;
    
    @EJB
    ProjectFacade projectFacade;
    
    TaskSchedulerManager taskManager;

    @Deployment
    public static JavaArchive deploy() {
        return AbstractSeagleTest.deployment();
    }

    @Before
    public void beforeTest() throws InterruptedException, ExecutionException {
        cleanUp();
    }

    public void cleanUp() {
        projectManager.delete(demoUrl);
        assertTrue(!projectManager.exists(demoUrl));
        Project project = projectManager.findByUrl(demoUrl);
        assertNull(project);
    }

    public AbstractGraphMetricTest() {

    }

}
