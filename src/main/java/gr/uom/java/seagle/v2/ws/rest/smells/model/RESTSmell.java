/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.ws.rest.smells.model;

import java.util.ArrayList;
import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Thodoris
 */
@XmlRootElement(name = "smell")
public class RESTSmell {
    
    private String name;
    
    @XmlElement
    private Collection<String> smellyContainers;

    public RESTSmell() {
        this.smellyContainers = new ArrayList();
    }
 
    public RESTSmell(String name) {
        this.name = name;
        this.smellyContainers = new ArrayList();
    }
      
    public void addSmellyContainer(String smellyContainer){
        smellyContainers.add(smellyContainer);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<String> getSmellyContainers() {
        return smellyContainers;
    }

}
