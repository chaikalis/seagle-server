package gr.uom.java.seagle.v2.metric;

import gr.uom.java.seagle.v2.AbstractSeagleActivator;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.se.util.manager.annotations.Activator;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.validation.ArgsCheck;

/**
 *
 * @author Elvis Ligu
 */
@Activator
public class AnalysisCompletedActivator extends AbstractSeagleActivator {

   private final EventManager eventManager;

   @ProvideModule
   public AnalysisCompletedActivator(
           @Property(name = NULLVal.NO_PROP) SeagleManager seagleManager,
           @Property(name = NULLVal.NO_PROP) EventManager eventManager) {
      super(seagleManager);
      ArgsCheck.notNull("eventManager", eventManager);
      this.eventManager = eventManager;
   }

   @Override
   protected void activate() {
      AnalysisCompletedListener listener = 
              new AnalysisCompletedListener(seagleManager, eventManager);
      listener.register();
   }
}
