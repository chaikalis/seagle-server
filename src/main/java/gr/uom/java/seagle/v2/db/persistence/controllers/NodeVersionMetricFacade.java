package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.NodeVersionMetric;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
public class NodeVersionMetricFacade extends AbstractFacade<NodeVersionMetric> {

   @PersistenceContext(unitName = "seaglePU")
   private EntityManager em;

   public NodeVersionMetricFacade() {
      super(NodeVersionMetric.class);
   }

   @Override
   protected EntityManager getEntityManager() {
      return em;
   }

   /**
    * Find all node metrics that has a node with the given version and the given
    * name.
    * 
    * @param nodeName
    *           the name of the node
    * @param version
    *           the version of the node
    * @return a list of node metrics where the node has the given name and
    *         version
    */
   public List<NodeVersionMetric> findByNodeNameAndVersion(String nodeName,
         Version version) {
      return JPAUtils.namedQueryEntityTwoParam(em, NodeVersionMetric.class,
            "NodeVersionMetric.findByNodeName", "nodeName", nodeName,
            "version", version);
   }

   /**
    * Find all node metrics, where node version is the same as the given one.
    * <p>
    * 
    * @param version
    *           the version of node
    * @return a list of node metrics where node has the given version.
    */
   public List<NodeVersionMetric> findByVersion(Version version) {
      ArgsCheck.notNull("version", version);
      String query = "NodeVersionMetric.findByVersion";
      String param = "version";
      return JPAUtils.namedQueryEntityOneParam(em, NodeVersionMetric.class,
            query, param, version);
   }
}
