package gr.uom.java.ast;


import java.util.*;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

import org.eclipse.jdt.core.dom.IBinding;

import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;

import org.eclipse.jdt.core.dom.SimpleName;

public class SystemObject {

    private List<ClassObject> classList;
	//Map that has as key the classname and as value
    //the position of className in the classNameList
    private Map<String, Integer> classNameMap;
    private Map<String, Integer> linesOfCodePerFilePathMap;
    private Map<MethodInvocationObject, FieldInstructionObject> getterMap;
    private Map<MethodInvocationObject, FieldInstructionObject> setterMap;
    private Map<MethodInvocationObject, FieldInstructionObject> collectionAdderMap;
    private Map<MethodInvocationObject, MethodInvocationObject> delegateMap;

    public SystemObject() {
        this.classList = new ArrayList<ClassObject>();
        this.classNameMap = new HashMap<String, Integer>();
        this.linesOfCodePerFilePathMap = new HashMap<String,Integer>();
        this.getterMap = new LinkedHashMap<MethodInvocationObject, FieldInstructionObject>();
        this.setterMap = new LinkedHashMap<MethodInvocationObject, FieldInstructionObject>();
        this.collectionAdderMap = new LinkedHashMap<MethodInvocationObject, FieldInstructionObject>();
        this.delegateMap = new LinkedHashMap<MethodInvocationObject, MethodInvocationObject>();
    }

    public Map<String, Integer> getLinesOfCodePerClassMap() {
        return linesOfCodePerFilePathMap;
    }
    
    public void addLinesOfCodeForAFile(String classPath, int linesOfCode) {
        this.linesOfCodePerFilePathMap.put(classPath, linesOfCode);
    }
    
    public int getLinesOfCodeForAFile(String classPath) {
        return this.linesOfCodePerFilePathMap.get(classPath);
    }

    public void addClass(ClassObject c) {
        classNameMap.put(c.getName(), classList.size());
        classList.add(c);
    }

    public void addClasses(List<ClassObject> classObjects) {
        for (ClassObject classObject : classObjects) {
            addClass(classObject);
        }
    }

    public void replaceClass(ClassObject c) {
        int position = getPositionInClassList(c.getName());
        if (position != -1) {
            classList.remove(position);
            classList.add(position, c);
        } else {
            addClass(c);
        }
    }

    public FieldInstructionObject containsGetter(MethodInvocationObject methodInvocation) {
        return getterMap.get(methodInvocation);
    }

    public FieldInstructionObject containsSetter(MethodInvocationObject methodInvocation) {
        return setterMap.get(methodInvocation);
    }

    public FieldInstructionObject containsCollectionAdder(MethodInvocationObject methodInvocation) {
        return collectionAdderMap.get(methodInvocation);
    }

    public MethodInvocationObject containsDelegate(MethodInvocationObject methodInvocation) {
        return delegateMap.get(methodInvocation);
    }

    public MethodObject getMethod(MethodInvocationObject mio) {
        ClassObject classObject = getClassObject(mio.getOriginClassName());
        if (classObject != null) {
            return classObject.getMethod(mio);
        }
        return null;
    }

    public MethodObject getMethod(SuperMethodInvocationObject smio) {
        ClassObject classObject = getClassObject(smio.getOriginClassName());
        if (classObject != null) {
            return classObject.getMethod(smio);
        }
        return null;
    }

    public ClassObject getClassObject(String className) {
        if(className.contains("<")){
            int pos = className.indexOf("<");
            className = className.substring(0, pos);
        }
        Integer pos = classNameMap.get(className);
        if (pos != null) {
            return getClassObject(pos);
        } else {
            return null;
        }
    }

    public ClassObject getClassObject(int pos) {
        return classList.get(pos);
    }

    public ListIterator<ClassObject> getClassListIterator() {
        return classList.listIterator();
    }

    public ListIterator<ClassObject> getPackageClassListIterator(String packageName) {
        List<ClassObject> packageClasses = new ArrayList<ClassObject>();
        for (ClassObject co : classList) {
            if (co.getName().startsWith(packageName)) {
                packageClasses.add(co);
            }
        }
        return packageClasses.listIterator();
    }

    public int getClassNumber() {
        return classList.size();
    }

    public int getPositionInClassList(String className) {
        Integer pos = classNameMap.get(className);
        if (pos != null) {
            return pos;
        } else {
            return -1;
        }
    }

    public Set<ClassObject> getClassObjects() {
        Set<ClassObject> classObjectSet = new LinkedHashSet<ClassObject>();
        classObjectSet.addAll(classList);
        return classObjectSet;
    }

    public Set<ClassObject> getClassObjects(IPackageFragmentRoot packageFragmentRoot) {
        Set<ClassObject> classObjectSet = new LinkedHashSet<ClassObject>();
        try {
            IJavaElement[] children = packageFragmentRoot.getChildren();
            for (IJavaElement child : children) {
                if (child.getElementType() == IJavaElement.PACKAGE_FRAGMENT) {
                    IPackageFragment packageFragment = (IPackageFragment) child;
                    classObjectSet.addAll(getClassObjects(packageFragment));
                }
            }
        } catch (JavaModelException e) {
            e.printStackTrace();
        }
        return classObjectSet;
    }

    public Set<ClassObject> getClassObjects(IPackageFragment packageFragment) {
        Set<ClassObject> classObjectSet = new LinkedHashSet<ClassObject>();
        try {
            ICompilationUnit[] packageCompilationUnits = packageFragment.getCompilationUnits();
            for (ICompilationUnit iCompilationUnit : packageCompilationUnits) {
                classObjectSet.addAll(getClassObjects(iCompilationUnit));
            }
        } catch (JavaModelException e) {
            e.printStackTrace();
        }
        return classObjectSet;
    }

    public Set<ClassObject> getClassObjects(ICompilationUnit compilationUnit) {
        Set<ClassObject> classObjectSet = new LinkedHashSet<ClassObject>();
        try {
            IType[] topLevelTypes = compilationUnit.getTypes();
            for (IType type : topLevelTypes) {
                classObjectSet.addAll(getClassObjects(type));
            }
        } catch (JavaModelException e) {
            e.printStackTrace();
        }
        return classObjectSet;
    }

    public Set<ClassObject> getClassObjects(IType type) {
        Set<ClassObject> classObjectSet = new LinkedHashSet<ClassObject>();
        String typeQualifiedName = type.getFullyQualifiedName('.');
        ClassObject classObject = getClassObject(typeQualifiedName);
        if (classObject != null) {
            classObjectSet.add(classObject);
        }
        try {
            IType[] nestedTypes = type.getTypes();
            for (IType nestedType : nestedTypes) {
                classObjectSet.addAll(getClassObjects(nestedType));
            }
        } catch (JavaModelException e) {
            e.printStackTrace();
        }
        return classObjectSet;
    }

    public MethodObject getMethodObject(IMethod method) {
        IType declaringType = method.getDeclaringType();
        ClassObject classObject = getClassObject(declaringType.getFullyQualifiedName('.'));
        if (classObject != null) {
            ListIterator<MethodObject> mi = classObject.getMethodIterator();
            while (mi.hasNext()) {
                MethodObject mo = mi.next();
                IMethod resolvedMethod = (IMethod) mo.getMethodDeclaration().resolveBinding().getJavaElement();
                if (method.isSimilar(resolvedMethod)) {
                    return mo;
                }
            }
        }
        return null;
    }

    public List<String> getClassNames() {
        List<String> names = new ArrayList<String>();
        for (int i = 0; i < classList.size(); i++) {
            names.add(getClassObject(i).getName());
        }
        return names;
    }

    private boolean nonEmptyIntersection(List<SimpleName> staticFieldUnion, List<SimpleName> staticFields) {
        for (SimpleName simpleName1 : staticFields) {
            for (SimpleName simpleName2 : staticFieldUnion) {
                if (simpleName1.resolveBinding().isEqualTo(simpleName2.resolveBinding())) {
                    return true;
                }
            }
        }
        return false;
    }

    private List<SimpleName> constructUnion(List<SimpleName> staticFieldUnion, List<SimpleName> staticFields) {
        List<SimpleName> initialStaticFields = new ArrayList<SimpleName>(staticFieldUnion);
        List<SimpleName> staticFieldsToBeAdded = new ArrayList<SimpleName>();
        for (SimpleName simpleName1 : staticFields) {
            boolean isContained = false;
            for (SimpleName simpleName2 : staticFieldUnion) {
                if (simpleName1.resolveBinding().isEqualTo(simpleName2.resolveBinding())) {
                    isContained = true;
                    break;
                }
            }
            if (!isContained) {
                staticFieldsToBeAdded.add(simpleName1);
            }
        }
        initialStaticFields.addAll(staticFieldsToBeAdded);
        return initialStaticFields;
    }

    private boolean allStaticFieldsWithinSystemBoundary(List<SimpleName> staticFields) {
        for (SimpleName staticField : staticFields) {
            IBinding binding = staticField.resolveBinding();
            if (binding.getKind() == IBinding.VARIABLE) {
                IVariableBinding variableBinding = (IVariableBinding) binding;
                ITypeBinding declaringClassTypeBinding = variableBinding.getDeclaringClass();
                if (declaringClassTypeBinding != null) {
                    if (getPositionInClassList(declaringClassTypeBinding.getQualifiedName()) == -1 && !declaringClassTypeBinding.isEnum()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (ClassObject classObject : classList) {
            sb.append(classObject.toString());
            sb.append("\n--------------------------------------------------------------------------------\n");
        }
        return sb.toString();
    }
}
