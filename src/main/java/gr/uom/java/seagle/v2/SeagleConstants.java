package gr.uom.java.seagle.v2;

import gr.uom.se.util.config.ConfigManager;
import gr.uom.se.util.validation.ArgsCheck;
import java.io.File;

/**
 * Constants used by seagle.
 * <p>
 * 
 * @author Theodore Chaikalis
 */
public class SeagleConstants {
    
    /**
    * The default value of seagle home in case there is not a configuration.
    * <p>
    */
   public static final String SEAGLE_HOME = "seagle";

   /**
    * The default location of configuration.
    * <p>
    */
   public static final String SEAGLE_CONFIG_FOLDER_NAME = "config";
   /**
    * The seagle domain, where most seagle properties will be stored.
    * <p>
    */
   public static final String SEAGLE_DOMAIN = "seagle_domain";

   /**
    * The name of the property of the seagle home folder.
    * <p>
    * Use {@link ConfigManager} to get the value of home folder.
    */
   public static final String SEAGLE_HOME_PROPERTY = "seagleHome";


   /**
    * The name of the property of the data home folder.
    * <p>
    * The data property should be relative to seagle home, and not an absolute
    * path.
    * <p>
    * Use {@link ConfigManager} to get the value of home folder.
    */
   public static final String DATA_HOME_PROPERTY = "dataHome";

   /**
    * The default value of data home in case there is not a configuration.
    * <p>
    */
   public static final String DATA_HOME = "data";

   /**
    * The name of the property of the projects home folder.
    * <p>
    * Projects home is relative to data home, and shouldn't be absolute path.
    * <p>
    * Use {@link ConfigManager} to get the value of home folder.
    */
   public static final String PROJECTS_HOME_PROPERTY = "projectsHome";

   /**
    * The default value of projects home in case there is not a configuration.
    * <p>
    */
   public static final String PROJECTS_HOME = "projects";

   /**
    * The name of the property of the repositories home folder.
    * <p>
    * Repositories home is relative to projects home, and shouldn't be absolute
    * path.
    * <p>
    * Use {@link ConfigManager} to get the value of home folder.
    */
   public static final String REPOS_HOME_PROPERTY = "reposHome";

   /**
    * The default value of repositories home in case there is not a
    * configuration.
    * <p>
    */
   public static final String REPOS_HOME = "repositories";

   /**
    * The name of the property of the checked out projects home folder.
    * <p>
    * Projects sources home is relative to projects home, and shouldn't be
    * absolute path.
    * <p>
    * Use {@link ConfigManager} to get the value of home folder.
    */
   public static final String PROJECTS_SOURCE_CODE_HOME_PROPERTY = "projectSourceCodeHome";

   /**
    * The default value of projects source home in case there is not a
    * configuration.
    * <p>
    */
   public static final String PROJECTS_SOURCE_CODE_HOME = "projectsSourceCode";
   
   /**
    * The jndi look up prefix.
    * <p>
    */
   public static final String JNDI_PREFIX = "jndi.";
   
   
   public static final String SMELL_DETECTOR_FEW_VALUE = "smellDetectionFew";
   
   public static final String SMELL_DETECTOR_MANY_VALUE = "smellDetectionMany";
   
   public static final String SMELL_DETECTOR_SEVERAL_VALUE = "smellDetectionSeveral";
   
   public static final String SMELL_DETECTOR_FE_LAA_LIMIT = "featureEnvyLAALevel";
   
   /**
    * Will return a property name 'jndi.<class name>' were class name
    * is the name of the type.
    * <p>
    * @param type to get the jndi property name for
    * @return the jndi property name
    */
   public static String getJNDIPropertyForType(Class<?> type) {
      ArgsCheck.notNull("type", type);
      return JNDI_PREFIX + type.getName();
   }
}
