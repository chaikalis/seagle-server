/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.MetricValue;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.ProjectMetric;
import gr.uom.java.seagle.v2.db.persistence.Property;
import gr.uom.java.seagle.v2.db.persistence.controllers.MetricValueFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectMetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.PropertyFacade;
import gr.uom.java.seagle.v2.metric.MetricManager;
import gr.uom.java.seagle.v2.metric.MetricRunInfo;
import gr.uom.java.seagle.v2.metric.imp.AbstractMetricTask;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.java.seagle.v2.scheduler.SchedulerConfig;
import gr.uom.se.util.concurrent.TaskType;
import gr.uom.se.vcs.VCSBranch;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.VCSResource;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;
import gr.uom.se.vcs.walker.CommitVisitor;
import gr.uom.se.vcs.walker.filter.commit.VCSCommitFilter;
import gr.uom.se.vcs.walker.filter.resource.VCSResourceFilter;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Elvis Ligu
 */
public class CommitCounterTask extends AbstractMetricTask {

   private int counter = 0;

   /**
    * @param handler
    * @param info
    */
   public CommitCounterTask(SeagleManager seagleManager, MetricRunInfo info) {

      super(seagleManager, info, getType(seagleManager),
            new HashSet<ExecutableMetric>(
                  Arrays.asList(RepoMetricEnum.COM_PROJ)));
   }

   private static TaskType getType(SeagleManager seagleManager) {
      return getType(seagleManager, SchedulerConfig.TASKTYPE_PARALLEL);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected void compute() {
      try (VCSRepository repo = getRepo()) {
         // Get the heads of branches
         Collection<VCSBranch> branches = repo.getBranches();
         Set<VCSCommit> heads = new HashSet<>();
         for (VCSBranch branch : branches) {
            heads.add(branch.getHead());
         }
         // Walk all heads and count the commits
         repo.walkAll(heads, new CommitVisitor() {

            @Override
            public boolean visit(VCSCommit entity) {
               counter++;
               return true;
            }

            @Override
            public <R extends VCSResource> VCSResourceFilter<R> getResourceFilter() {
               return null;
            }

            @SuppressWarnings("unchecked")
            @Override
            public VCSCommitFilter getFilter() {
               return null;
            }
         }, true);
      } catch (VCSRepositoryException e) {
         throw new RuntimeException(e);
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected void updateDB() {
      updateMetricBD();
      updateMetricProperties();
   }

   private void updateMetricBD() {
      // Should update the db
      MetricManager metrics = resolveComponentOrManager(MetricManager.class);
      Metric metric = metrics.getMetricByMnemonic(RepoMetricEnum.COM_PROJ
            .getMnemonic());
      if (metric == null) {
         throw new RuntimeException("metric " + RepoMetricEnum.COM_PROJ
               + " was not found in db");
      }
      ProjectManager projects = resolveComponentOrManager(ProjectManager.class);
      String purl = getProjectUrl();
      Project project = projects.findByUrl(purl);
      if (project == null) {
         throw new RuntimeException("project from " + purl
               + " was not found in db");
      }
      ProjectMetricFacade projectMetrics = resolveComponentOrManager(ProjectMetricFacade.class);
      projectMetrics.upsertMetric(project, metric, counter);
   }

   private void updateMetricProperties() {
      // Get the property facade
      PropertyFacade properties = resolveComponentOrManager(PropertyFacade.class);
      // Get project url
      String purl = getProjectUrl();
      // Get all default branch properties
      Collection<Property> bProperties = ProjectBranchPropertyUpdater
            .getBranchProperties(properties, purl);

      // For each branch property we should check the db if
      // it has a branch property for our metric, if not we should
      // create it, if yes we should update it
      String domain = purl;
      for (Property bProperty : bProperties) {
         String bName = bProperty.getName();
         // Branch property is like 'BRANCH.bid'
         String bid = ProjectBranchPropertyUpdater.extractBid(bName);
         String name = CounterMetricHandler.getMetricBranchPropertyName(bid);
         String value = bProperty.getValue();
         // Find the metric branch property here
         Property mbProperty = properties.getPropertyByName(domain, name);

         // If not found create it
         // Otherwise update it
         if (mbProperty == null) {
            // Create the property here
            properties.create(domain, name, value);
         } else {
            // If the values are different then we should update
            // avoiding writing into db if values are the same
            String myValue = mbProperty.getValue();
            if (!myValue.equals(value)) {
               mbProperty.setValue(myValue);
               properties.edit(mbProperty);
            }
         }
      }
   }
}
