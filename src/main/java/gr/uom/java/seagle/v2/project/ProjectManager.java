package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionProvider;
import java.util.List;
import javax.ejb.Local;

/**
 * A Manager that handles operations relevant with local repository management, 
 * and to their respective project DB entities.
 * <p>
 * Use this manager to gain access in the locally stored repositories and the
 * source code of the projects, as well as to download (clone) remote
 * repositories. Also use this manager to gain access to DB for project
 * entities.
 * <p>
 * This manager is generally an extension of interfaces just to provide
 * a LOCAL interface for a stateless bean that should be single entry point for
 * operations on projects.
 *
 * @author Theodore Chaikalis
 * @author Elvis Ligu
 */
@Local
public interface ProjectManager extends RepositoryManager, ProjectDBManager {
    
    public abstract ConnectedVersionProvider getVersions(String projectRepoURL);
    
   
    
}
