
package gr.uom.java.seagle.v2.analysis.graph.javaGraph;

import gr.uom.java.seagle.v2.analysis.graph.GraphVisitor;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.analysis.project.evolution.JavaProject;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Theodore Chaikalis
 */
public class JavaClassGraph extends SoftwareGraph<JavaNode, JavaEdge> implements Serializable {

    private Set<JavaPackage> packages;
    
    public JavaClassGraph(){
        packages = new HashSet<>();
    }
    
    public void addPackage(JavaPackage _package) {
        packages.add(_package);
    }
    
    public JavaPackage getPackage(String packageName) {
        for(JavaPackage jp : packages) {
            if(jp.getName().equals(packageName)){
                return jp;
            }
        }
        return null;
    }
    
    public Collection<JavaNode> getNodes(){
        for(JavaNode jn : this.getVertices()) {
            jn.toString();
        }
        return null;
    }

    @Override
    public JavaNode getNode(String name) {
        return super.getNode(name);
    }

    @Override
    public void accept(GraphVisitor graphVisitor) {
        graphVisitor.visit(this);
    }


}
