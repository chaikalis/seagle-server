package gr.uom.java.seagle.v2.db.persistence;

import gr.uom.se.util.validation.ArgsCheck;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author elvis
 */
@Entity
@Table(name = "version")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Version.findAll", query = "SELECT v FROM Version v"),
    @NamedQuery(name = "Version.findById", query = "SELECT v FROM Version v WHERE v.id = :id"),
    @NamedQuery(name = "Version.findByName", query = "SELECT v FROM Version v WHERE v.name = :name"),
    @NamedQuery(name = "Version.findByDate", query = "SELECT v FROM Version v WHERE v.date = :date"),
    @NamedQuery(name = "Version.findByCommitIDandProject", query = "SELECT v FROM Version v JOIN v.project p WHERE v.commitID = :cid AND p = :project"),
    @NamedQuery(name = "Version.findByCommitID", query = "SELECT v FROM Version v WHERE v.commitID = :commitID"),
    @NamedQuery(name = "Version.findByProject", query = "SELECT v FROM Version v WHERE v.project = :project"),
    @NamedQuery(name = "Version.findByProjectURL", query = "SELECT v FROM Version v JOIN v.project p WHERE p.remoteRepoPath = :url")})
public class Version implements Serializable, Comparable<Version> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "name")
    private String name;

    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1024)
    @Column(name = "commitID")
    private String commitID;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "version", orphanRemoval = true)
    private Collection<Node> nodes;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "version", orphanRemoval = true)
    private Collection<NodeVersionMetric> nodeVersionMetrics;

    @JoinColumn(name = "project_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Project project;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "version", orphanRemoval = true)
    private Collection<ProjectVersionMetric> projectVersionMetrics;

    @ManyToMany(mappedBy = "authorVersions")
    private Collection<Developer> authors;

    @ManyToMany(mappedBy = "committerVersions")
    private Collection<Developer> committers;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "version", orphanRemoval = true)
    private Collection<Method> methods;
    

    public Version() {
        methods = new HashSet<>();
    }

    public Version(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCommitID() {
        return commitID;
    }

    public void setCommitID(String commitID) {
        this.commitID = commitID;
    }

    @XmlTransient
    public Collection<Node> getNodes() {
        if (nodes == null) {
            return Collections.emptyList();
        }
        return nodes;
    }

    @XmlTransient
    public Collection<NodeVersionMetric> getNodeVersionMetrics() {
        return nodeVersionMetrics;
    }

    public void setNodeVersionMetrics(
            Collection<NodeVersionMetric> nodeVersionMetrics) {
        this.nodeVersionMetrics = nodeVersionMetrics;
    }

    @XmlTransient
    public Collection<Developer> getAuthors() {
        return authors;
    }

    public void setAuthors(Collection<Developer> authors) {
        this.authors = authors;
    }

    @XmlTransient
    public Collection<Developer> getCommitters() {
        return committers;
    }

    public void setCommitters(Collection<Developer> committers) {
        this.committers = committers;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @XmlTransient
    public Collection<ProjectVersionMetric> getProjectVersionMetrics() {
        return projectVersionMetrics;
    }

    public void setProjectVersionMetrics(
            Collection<ProjectVersionMetric> projectVersionMetrics) {
        this.projectVersionMetrics = projectVersionMetrics;
    }

    /**
     * Add a computed metric to this version.
     * <p>
     * Note these metrics are not version specific.
     *
     * @param metric to be added to project
     */
    public void addProjectVersionMetric(ProjectVersionMetric metric) {
        ArgsCheck.notNull("version metric", metric);
        if (this.projectVersionMetrics == null) {
            projectVersionMetrics = new HashSet<>();
        }
        projectVersionMetrics.add(metric);
    }

    public void addNodeVersionMetric(NodeVersionMetric metric) {
        ArgsCheck.notNull("version metric", metric);
        if (this.nodeVersionMetrics == null) {
            nodeVersionMetrics = new HashSet<>();
        }
        nodeVersionMetrics.add(metric);
    }

    public Collection<Method> getMethods() {
        return methods;
    }

    public void setMethods(Collection<Method> methods) {
        this.methods = methods;
    }
    
    public void addMethod(Method m){
        this.methods.add(m);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
      // TODO: Warning - this method won't work in the case the id fields are
        // not set
        if (!(object instanceof Version)) {
            return false;
        }
        Version other = (Version) object;
        if ((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gr.uom.java.seagle.db.persistence.v2.Version[ id=" + id + " ] ,[name = "+name+ " ]";
    }

    @Override
    public int compareTo(Version other) {
        return this.id.compareTo(other.id);
    }

}
