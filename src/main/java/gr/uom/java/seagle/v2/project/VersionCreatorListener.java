/**
 * 
 */
package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.AbstractSeagleListener;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.db.persistence.controllers.VersionFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.se.util.context.ContextManager;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventType;
import gr.uom.se.util.module.ModuleContext;
import gr.uom.se.util.validation.ArgsCheck;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionNameProvider;
import gr.uom.se.vcs.analysis.version.provider.VersionNameProvider;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * A listener that will create each version after he receives a project
 * clone/update event.
 * 
 * @author Elvis Ligu
 */
public class VersionCreatorListener extends AbstractSeagleListener {

   /**
    * @param seagleManager
    * @param eventManager
    */
   protected VersionCreatorListener(SeagleManager seagleManager,
         EventManager eventManager) {
      super(seagleManager, eventManager);
   }

   @Override
   protected void register() {
      super.register();
   }

   @Override
   protected void acceptEvent(EventType type, EventInfo info) {
      ProjectInfo pinfo = (ProjectInfo) info.getDescription();
      if (type.equals(ProjectEventType.DB_INSERTED)
            || type.equals(ProjectEventType.DB_UPDATED)) {
         updateVersions(pinfo);
      } else {
         throw new IllegalStateException("Event " + type
               + " is not supposed to be received");
      }
   }

   /**
    * @param pinfo
    */
   private void updateVersions(ProjectInfo pinfo) {

      ProjectManager projectManager = resolveComponentOrManager(ProjectManager.class);

      // Open the repository, resolve its versions and create them to db
      try (VCSRepository repo = projectManager.getRepository(pinfo
            .getRemoteUrl())) {
         ConnectedVersionNameProvider versionProvider = getVersions(repo);
         updateDBVersions(versionProvider, pinfo.getRemoteUrl());
      }
   }

   private void updateDBVersions(VersionNameProvider versionProvider,
         String projectUrl) {

      Project project = resolveComponentOrManager(ProjectManager.class)
            .findByUrl(projectUrl);

      VersionFacade versionFacade = resolveComponentOrManager(VersionFacade.class);

      ArgsCheck.notNull("project", project);
      Iterator<VCSCommit> it = versionProvider.getCommits().iterator();

      while (it.hasNext()) {
         VCSCommit vcommit = it.next();
         Version version = getVersionForID(project, vcommit.getID());
         if (version != null) {
            continue;
         }
         version = new Version();
         version.setCommitID(vcommit.getID());
         version.setName(versionProvider.getName(vcommit));
         version.setProject(project);
         version.setDate(vcommit.getCommitDate());
         project.addVersion(version);
         versionFacade.create(version);
      }
   }

   /**
    * Get a version name provider for the given project url.
    * 
    * @param projectUrl
    * @return
    */
   private ConnectedVersionNameProvider getVersions(VCSRepository repo) {
      ContextManager cm = resolveComponentOrManager(ContextManager.class);
      ModuleContext context = cm.lookupContext(repo, ModuleContext.class);
      ArgsCheck.notNull("context", context);
      return context.load(ConnectedVersionNameProvider.class);
   }

   /**
    * Get the version with the given commit id.
    * 
    * @param project
    * @param cid
    * @return
    */
   private Version getVersionForID(Project project, String cid) {
      List<Version> versions = resolveComponentOrManager(VersionFacade.class)
            .findByCommitID(project, cid);
      if (versions.isEmpty()) {
         return null;
      }
      return versions.get(0);
   }

   @Override
   protected Set<EventType> getTypes() {
      Set<EventType> types = new HashSet<>();
      types.add(ProjectEventType.DB_INSERTED);
      types.add(ProjectEventType.DB_UPDATED);
      return types;
   }

}
