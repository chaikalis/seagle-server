package gr.uom.java.seagle.v2.db.persistence;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity linking a project with a version and a calculated metric.
 * <p>
 * @author Elvis Ligu
 */
@Entity
@Table(name = "project_version_metric")
@XmlRootElement
@IdClass(ProjectVersionMetricPK.class)
@NamedQueries({
    @NamedQuery(name = "ProjectVersionMetric.findAll", query = "SELECT p FROM ProjectVersionMetric p"),
    @NamedQuery(name = "ProjectVersionMetric.findByVersionAndMetric", query = "SELECT p FROM ProjectVersionMetric p JOIN p.metricValue mv WHERE mv.metric = :metric and p.version = :version"),
    @NamedQuery(name = "ProjectVersionMetric.findByVersion", query = "SELECT p FROM ProjectVersionMetric p WHERE p.version = :version")})
public class ProjectVersionMetric implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @ManyToOne(optional = false)
    private Version version;

    @Id
    @OneToOne(cascade = CascadeType.ALL, optional = false)
    private MetricValue metricValue;

    public ProjectVersionMetric() {
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public MetricValue getMetricValue() {
        return metricValue;
    }

    public void setMetricValue(MetricValue metricValue) {
        this.metricValue = metricValue;
    }

}
