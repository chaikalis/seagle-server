package gr.uom.java.seagle.v2.analysis.metrics.sourceCode;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.ConstructorObject;
import gr.uom.java.ast.MethodObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.java.seagle.v2.analysis.project.evolution.JavaProject;
import gr.uom.java.seagle.v2.db.persistence.Method;
import gr.uom.java.seagle.v2.db.persistence.Version;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author Thodoris Chaikalis
 */
public class LAA extends AbstractJavaExecutableMetric {

    public static final String MNEMONIC = "LAA";

    public LAA(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(SystemObject systemObject, JavaProject javaProject) {
        Map<String, Double> valuePerClass = getLAAForAllClasses(systemObject, javaProject);
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, javaProject);
        storeProjectLevelAggregationMetricInMemory(getMnemonic(), javaProject, valuePerClass, MetricAggregationStrategy.Average);
    }

    private Map<String, Double> getLAAForAllClasses(SystemObject systemObject, JavaProject javaProject) {
        Map<String, Double> valuePerClass = new LinkedHashMap<>();
        ListIterator<ClassObject> classIterator = systemObject.getClassListIterator();
        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            String className = classObject.getName();
            List<Method> methods = new ArrayList<>();
            double locality = calculateLocality(classObject, systemObject, methods, javaProject.getProjectVersion());
            valuePerClass.put(className, locality);
            javaProject.putMetricValuesForMethodsOfAClass(className, methods);
        }
        return valuePerClass;
    }

    private float calculateLocality(ClassObject classObject, SystemObject systemObject, List<Method> methods, Version version) {
        float foreignVariablesAccessed = 0;
        float numberOfFieldsAccessed = 0;
        float totalVariablesAccessed = 0;

        for (MethodObject method : classObject.getMethodList()) {
            Method simpleMethod = new Method(method, version);
            ConstructorObject con = method.getConstructorObject();
            int foreignVariablesAccessedFromMethod = 0;
            int nativeVariablesAccessedFromMethod = 0;

            foreignVariablesAccessedFromMethod += con.getUsedAndDefinedFieldsOfForeignClasses().size();
            foreignVariablesAccessedFromMethod += con.getForeignGetters(systemObject).size();

            nativeVariablesAccessedFromMethod += con.getNumberOfUsedFieldsOfNativeClass(systemObject);

            foreignVariablesAccessed += foreignVariablesAccessedFromMethod;
            numberOfFieldsAccessed += nativeVariablesAccessedFromMethod;

            double methodLocality = 0;
            if (foreignVariablesAccessed > 0) {
                methodLocality = (double) nativeVariablesAccessedFromMethod / (double) foreignVariablesAccessedFromMethod;
            }

            simpleMethod.putMetricValue(getMnemonic(), methodLocality);
            methods.add(simpleMethod);
        }

        totalVariablesAccessed = foreignVariablesAccessed + classObject.getNumberOfMethodsThatAccessAField();

        float locality = 0;
        if (totalVariablesAccessed > 0) {
            locality = numberOfFieldsAccessed / totalVariablesAccessed;
        }
        return locality;
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "The number of attributes from the method’s deﬁnition class, "
                + "divided by the total number of variables accessed "
                + "(including attributes used via accessor methods, see ATFD), "
                + "whereby the number of local attributes accessed is computed in conformity with the LAA speciﬁcations ";
    }

    @Override
    public String getName() {
        return "Locality of Atribute Access";
    }

}
