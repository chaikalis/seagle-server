package gr.uom.java.seagle.v2.analysis.metrics;

import gr.uom.java.seagle.v2.AbstractSeagleResolver;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author Thodoris Chaikalis
 */
public abstract class AbstractAnalysisMetric extends AbstractSeagleResolver implements ExecutableMetric {

    private final Logger logger = Logger.getLogger(AbstractAnalysisMetric.class.getName());
    
    public AbstractAnalysisMetric(SeagleManager seagleManager) {
        super(seagleManager);
    }

    protected void storeValuesForAllNodesInMemory(String mnemonic, Map<String, Double> valuePerClass, SoftwareProject project) {
        project.putMetricValuePerClass(mnemonic, valuePerClass);
    }

    protected void storeProjectLevelAggregationMetricInMemory(String metricMnemonic, SoftwareProject project, Map<String, Double> valuePerClass, MetricAggregationStrategy aggregationStrategy) {
        Number aggregatedValue = aggregationStrategy.getAggregatedValue(valuePerClass.values());
        project.putProjectLevelMetric(metricMnemonic, aggregatedValue);
    }

}
