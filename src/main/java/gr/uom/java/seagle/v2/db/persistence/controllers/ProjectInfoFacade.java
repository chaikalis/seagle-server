package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.ProjectInfo;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * A simple project info service used for basic CRUD operations.
 * <p>
 * This class should be run within a container which supports
 * managed transactions. Each db operation is supposed to be run
 * within a transaction provided by the container (DEFAULT = required).
 * For more control over operations provided by this class the calling
 * method should provide its own support of transactions.
 * 
 * @author Elvis Ligu
 */
@Stateless
public class ProjectInfoFacade extends AbstractFacade<ProjectInfo> {

    @PersistenceContext(unitName = "seaglePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Create an instance of this service by specifying the entity
     * type of class {@link ProjectInfo}.
     */
    public ProjectInfoFacade() {
        super(ProjectInfo.class);
    }
}
