/**
 * 
 */
package gr.uom.java.seagle.v2;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.enterprise.concurrent.ManagedExecutorService;

/**
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
@Stateless
public class ResourceProvider {

   @Resource
   ManagedExecutorService executorService;
   
   public ManagedExecutorService getExecutorService() {
      return executorService;
   }
}
