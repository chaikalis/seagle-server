package gr.uom.java.seagle.v2.analysis.smellDetection.events;

import gr.uom.java.seagle.v2.AbstractSeagleListener;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisEventType;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventType;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thodoris Chaikalis
 */
public class SmellDetectionListener extends AbstractSeagleListener {

    private static final Logger logger = Logger.getLogger(SmellDetectionListener.class.getName());

    public SmellDetectionListener(SeagleManager seagleManager, EventManager eventManager) {
        super(seagleManager, eventManager);
    }

    @Override
    protected void register() {
        super.register();
    }

    @Override
    protected void acceptEvent(EventType type, EventInfo info) {
        if (type.equals(SmellDetectionEventType.SMELL_DETECTION_REQUESTED)) {
            trigger(info);
        } else if (type.equals(SmellDetectionEventType.SMELL_DETECTION_ENDED)) {
            logger.log(Level.INFO, "Smell Detection END recieved. {0}", info.getDescription());
        }

    }

    private void trigger(EventInfo info) {
        AnalysisRequestInfo pinfo = (AnalysisRequestInfo) info.getDescription();
        logger.log(Level.INFO, "Smell Detection Request recieved for project {0}", pinfo.getRemoteProjectUrl());
        SmellDetectionManager smellDetector = new SmellDetectionManager(seagleManager, pinfo);
        smellDetector.compute();
    }
}
