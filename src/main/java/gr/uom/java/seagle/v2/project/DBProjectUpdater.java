package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventListener;
import gr.uom.se.util.event.EventType;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.Date;

/**
 *
 * @author elvis
 */
public class DBProjectUpdater implements EventListener {

   private final SeagleManager seagleManager;

   public DBProjectUpdater(SeagleManager seagleManager) {
      ArgsCheck.notNull("seagleManager", seagleManager);
      this.seagleManager = seagleManager;
   }

   @Override
   public void respondToEvent(Event event) {
      EventType eventType = event.getType();
      if (eventType instanceof ProjectEventType) {
         ProjectInfo info = (ProjectInfo) event.getInfo().getDescription();
         ProjectEventType pType = (ProjectEventType) eventType;

         switch (pType) {
         case CLONE:
            createProject(info, event.getInfo().getStartDate());
            break;
         case REMOVE:
            removeProject(info);
            break;
         case UPDATE:
            updateProject(info);
            break;
         default:
            break;
         }
      }
   }

   private void createProject(ProjectInfo info, Date startDate) {
      
      // Resolve project service
      ProjectDBManager service = resolveComponent(ProjectDBManager.class);
      // Get the project by name and check that it should
      // not be in the db
      Project result = service.findByName(info.getName());
      if (result != null) {
         throw new IllegalStateException("There is a project in DB with name "
               + info.getName());
      }
      
      Project project = new Project();
      project.setName(info.getName());
      project.setRemoteRepoPath(info.getRemoteUrl());

      // Create project info
      gr.uom.java.seagle.v2.db.persistence.ProjectInfo pInfo = 
            new gr.uom.java.seagle.v2.db.persistence.ProjectInfo();
      
      pInfo.setDateInserted(startDate);

      // Set info to project
      project.setProjectInfo(pInfo);
      pInfo.setProject(project);

      // Create project in db
      service.create(project);
      
      // Now trigger an event for the changes we made to db
      triggerEvent(ProjectEventType.DB_INSERTED, info.getName(),
            info.getRemoteUrl(), info.getLocalFolder());

   }

   private void removeProject(ProjectInfo info) {
      // Resolve user transaction
      // UserTransaction utx = resolveComponent(UserTransaction.class);
      // Resolve project service
      ProjectDBManager service = resolveComponent(ProjectDBManager.class);
      // Get the project by name and check that it should
      // not be in the db
      Project project = service.findByName(info.getName());
      // If the project is already removed from db
      // we do nothing
      if (project == null) {
         return;
      }
      
      // Remove the project from db
      service.remove(project);

      // Now trigger an event for the changes we made to db
      triggerEvent(ProjectEventType.DB_REMOVED, info.getName(),
            info.getRemoteUrl(), info.getLocalFolder());
   }

   private void updateProject(ProjectInfo info) {

      ProjectDBManager service = resolveComponent(ProjectDBManager.class);
      // Get the project by name and check that it should
      // not be in the db
      Project project = service.findByName(info.getName());
      if (project == null) {
         throw new IllegalStateException("The project is not in DB with name "
               + info.getName());
      }
      
      project.getProjectInfo().setLastUpdate(new Date());

      // Now trigger an event
      triggerEvent(ProjectEventType.DB_UPDATED, info.getName(),
            info.getRemoteUrl(), info.getLocalFolder());
   }

   private void triggerEvent(ProjectEventType type, String name,
         String repoUrl, String localUrl) {
      EventManager eventManager = seagleManager
            .resolveComponent(EventManager.class);
      ProjectInfo pInfo = new ProjectInfo(name, repoUrl, localUrl);
      EventInfo eInfo = new EventInfo(pInfo, new Date());
      Event event = type.newEvent(eInfo);
      eventManager.trigger(event);
   }

   private <T> T resolveComponent(Class<T> type) {
      T t = seagleManager.getManager(type);
      
      if (t == null) {
         throw new RuntimeException("Can not resolve component " + type
               + " from seagle manager");
      }
      return t;
   }
}
