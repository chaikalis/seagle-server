package gr.uom.java.seagle.v2.analysis.metrics.sourceCode;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.FieldInstructionObject;
import gr.uom.java.ast.MethodObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.java.seagle.v2.analysis.project.evolution.JavaProject;
import gr.uom.java.seagle.v2.db.persistence.Method;
import gr.uom.java.seagle.v2.db.persistence.Version;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author Thodoris Chaikalis
 */
public class FDP extends AbstractJavaExecutableMetric {

    public static final String MNEMONIC = "FDP";

    public FDP(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(SystemObject systemObject, JavaProject javaProject) {
        Map<String, Double> valuePerClass = getFPDForAllClasses(systemObject, javaProject);
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, javaProject);
        storeProjectLevelAggregationMetricInMemory(getMnemonic(), javaProject, valuePerClass, MetricAggregationStrategy.Average);
    }

    private Map<String, Double> getFPDForAllClasses(SystemObject systemObject, JavaProject javaProject) {
        Map<String, Double> valuePerClass = new LinkedHashMap<>();
        ListIterator<ClassObject> classIterator = systemObject.getClassListIterator();
        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            String className = classObject.getName();
            List<Method> methods = new ArrayList<>();
            calculateFDP(classObject, systemObject, valuePerClass, methods, javaProject.getProjectVersion());
            javaProject.putMetricValuesForMethodsOfAClass(className, methods);
        }
        return valuePerClass;
    }

    public void calculateFDP(ClassObject classObject, SystemObject systemObject, Map<String, Double> valuePerClass, List<Method> methods, Version version) {
        int fieldsThatReferenceForeignClasses = 0;
         for (MethodObject method : classObject.getMethodList()) {
              Method simpleMethod = new Method(method, version);
             int usedFieldsInThisMethodThatReferenceForeignClasses = 0;
             List<FieldInstructionObject> fieldInstructions = method.getConstructorObject().getFieldInstructions();
             for(FieldInstructionObject fio : fieldInstructions){
                 String fieldOwnerClass = fio.getOwnerClass();
                 if(!fieldOwnerClass.equals(classObject.getName())){
                     if(systemObject.getClassObject(fieldOwnerClass) != null) {
                         usedFieldsInThisMethodThatReferenceForeignClasses++;
                     }
                 }
             }
             simpleMethod.putMetricValue(getMnemonic(), usedFieldsInThisMethodThatReferenceForeignClasses);
             methods.add(simpleMethod);
             fieldsThatReferenceForeignClasses += usedFieldsInThisMethodThatReferenceForeignClasses;
         }
        valuePerClass.put(classObject.getName(), (double)fieldsThatReferenceForeignClasses);
        
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return " Foreign Data Providers are the used attributes that are "
                + "instances of foreign classes, "
                + "which are called to access foreign data. ";
    }

    @Override
    public String getName() {
        return "Foreign Data Provider";
    }

}
