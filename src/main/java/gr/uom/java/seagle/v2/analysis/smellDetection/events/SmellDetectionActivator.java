package gr.uom.java.seagle.v2.analysis.smellDetection.events;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisActivator;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.metric.AbstractMetricActivator;
import gr.uom.java.seagle.v2.metric.MetricManager;
import gr.uom.se.util.manager.annotations.Activator;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.validation.ArgsCheck;

/**
 *
 * @author Theodore Chaikalis
 */
@Activator(
        dependencies = AnalysisActivator.class
)
public class SmellDetectionActivator extends AbstractMetricActivator {

    private final EventManager eventManager;

    @ProvideModule
    public SmellDetectionActivator(
            @Property(name = NULLVal.NO_PROP) EventManager evm,
            @Property(name = NULLVal.NO_PROP) SeagleManager seagleManager,
            @Property(name = NULLVal.NO_PROP) MetricManager metricManager) {

        super(seagleManager, metricManager);
        ArgsCheck.notNull("evm", evm);
        this.eventManager = evm;
    }

    @Override
    protected void activate() {
       SmellDetectionListener smellDetectionListener = new SmellDetectionListener(seagleManager, eventManager);
       eventManager.addListener(SmellDetectionEventType.SMELL_DETECTION_REQUESTED, smellDetectionListener);   
       eventManager.addListener(SmellDetectionEventType.SMELL_DETECTION_ENDED, smellDetectionListener);   
    }


}
