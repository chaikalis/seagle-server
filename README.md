![SEAgle_logo_net_300x79.png](https://bitbucket.org/repo/B4y9e5/images/586573375-SEAgle_logo_net_300x79.png)

# What is this? #
## Seagle is an online platform for Software Repository Mining, and evolution analysis of JAVA projects. It facilitates the collection of project related data, organizes relevant information in comprehensible reports and provides a useful tool for empirical research. ##

# Overview #
## The development of the platform was driven by the following key issues: ##
* the platform should be easy to use. To this end we opted for a Web based platform enabling users to analyze a repository by a single click (either selection of an already analyzed project or by providing the git repository URI).
* software repositories encompass a project’s history. As a result, all reported information spans across all available versions, i.e. constitutes a form of software evolution analysis.
* Any software system has several facets. Therefore, we offer multiple views concerning commit-related metrics, source code metrics and graph based metrics.
* Empirical studies very often focus on the investigation of relations among variables. To satisfy this need we offer direct correlation analysis between any two monitored variables. (for this reason the x-axis is common on all diagrams and represents software versions)
* Contemporary software repositories are extremely large in size. To confront this challenge, we optimized the process of extracting commit-related metrics, which are demanding since they involved the analysis of thousands of commits.


# Build Instructions #

*We suggest that the best way to extend Seagle is to fork this repo, work on your own repository freely, experimentize on everything you want, and when you are sure that your code works, create a pull request.*

Seagle is comprised of multiple components. In this repository lies the server component that handles the repository cloning, performs all the metric calculations and stores the results in the database.

If you what to fork seagle-server and develop your own analysis or metric you can follow the following procedure. We hope you will find it easy.

## Database ##
Seagle is heavily depended on a Database. So if you want to develop locally you should first download an install MySql Developer Edition. 

You can download FREE MySql Community server from [here.](https://dev.mysql.com/downloads/mysql/5.6.html)

For the easy creation of databases, data and user manipulation we propose the MySql Workbench as it is a nice and free graphical database management system. You can download it from [here.](https://dev.mysql.com/downloads/workbench/)

Create a database with the name: 
```
#!sql

seagle2db
```
Create a user with the name: 
```
#!sql

seagleuser
```
user password: 


```
#!sql

Se@gle_2016@MySql
```

then give the user "seagleuser" privilleges to read, write and create tables on the seagle2db


```
#!sql

GRANT ALL PRIVILEGES ON seagle2db.* TO 'seagleuser'@'%';
```


or by using the MySql Workbench:

![mysql-workbench-tutorial-1.PNG](https://bitbucket.org/repo/B4y9e5/images/238664673-mysql-workbench-tutorial-1.PNG)

and for granting privileges

![mysql-workbench-tutorial-2.PNG](https://bitbucket.org/repo/B4y9e5/images/2580843475-mysql-workbench-tutorial-2.PNG)



# Internal dependencies #

Seagle need an in-house built library. So it should be cloned and built separately.

Clone this repository https://github.com/eligu/se-uom-vcs.git, navigate to its folder and build it by running 


```
#!java

mvn install
```

if build fails, try building with test skipping:


```
#!java

mvn install -Dmaven.test.skip=true
```


# Java Version #

Seagle currently runs ONLY in Java 7. So make sure that maven (mvn) and your IDE of choice run and compile code only in Java 7


# Server #

You can use Glassfish Server version 4.1.0. IMPORTANT! NOT WORKING ON 4.1.1

If you use Netbeans as your IDE you should not worry about the application server. Netbeans comes with the Glassfish App server embedded. Only make sure it is not version 4.1.1.


You can also use the latest [Payara distribution.](http://www.payara.fish/)



# Seagle configuration files #

Seagle is fully configured by its config files which initially lie in the folder: ~CODE_ROOT/src/main/resources/seagle/config/

You should modify the initial files and especially the file: *seagle_domain.properties
*

There you can define a Gmail Account for Email notification and also define your desired folder names for the project repositories and the source code that will be downloaded.


You are now ready to build and run Seagle!