package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventListener;
import gr.uom.se.util.event.EventType;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.Date;

/**
 *
 * @author elvis
 */
public class DBProjectActionUpdater implements EventListener {

   private final SeagleManager seagleManager;

   public DBProjectActionUpdater(SeagleManager seagleManager) {
      ArgsCheck.notNull("seagleManager", seagleManager);
      this.seagleManager = seagleManager;
   }

   @Override
   public void respondToEvent(Event event) {
      EventType eventType = event.getType();
      if (eventType instanceof ProjectEventType) {
         ProjectInfo info = (ProjectInfo) event.getInfo().getDescription();
         ProjectEventType pType = (ProjectEventType) eventType;

         switch (pType) {
         case DB_INSERTED:
            createInsertAction(info, event.getInfo().getStartDate());
            break;
         // case DB_REMOVED: do nothing, as long as the project
         // was removed from db we can not create an action for
         // that project, because it will throw a DB exception
         case DB_UPDATED:
            createUpdateAction(info, event.getInfo().getStartDate());
            break;
         default:
            break;
         }
      }
   }

   private void createInsertAction(ProjectInfo info, Date startDate) {
      createAction(info, startDate, DBProjectActionType.PROJECT_INSERTED);
   }

   private void createUpdateAction(ProjectInfo info, Date startDate) {
      createAction(info, startDate, DBProjectActionType.PROJECT_UPDATED);
   }

   private void createAction(ProjectInfo info, Date startDate,
         DBProjectActionType ptype) {
      ProjectDBManager dbManager = resolveManager(ProjectDBManager.class);

      dbManager.createTimelineForProjectName(info.getName(), ptype.name(),
            startDate);
   }

   private <T> T resolveManager(Class<T> type) {
      T t = seagleManager.getManager(type);
      if (t == null) {
         throw new RuntimeException("Can not resolve manager " + type
               + " from seagle manager");
      }
      return t;
   }
}
