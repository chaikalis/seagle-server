/**
 * 
 */
package gr.uom.java.seagle.v2;

import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventListener;
import gr.uom.se.util.event.EventType;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.Set;

/**
 * An abstract seagle listener that can listen to seagle events.
 * <p>
 * Although not required this class can be a base class for any listener that
 * must listen at certain event types. In order for suclasses to define the type
 * of events they can listen they should override {@link #getTypes()} method. If
 * this method returns null that means this listener will listen to all events
 * coming from event manager. The default implementation will listen to all
 * events. Because it will register all the defined events that are returned
 * from {@link #getTypes()}. Note subclasses should call {@link #register()} in
 * order to register to events, and when this method is called they should
 * specify any event type that they wants to listen to.
 * 
 * @author Elvis Ligu
 */
public abstract class AbstractSeagleListener extends AbstractSeagleResolver
      implements EventListener {

   private final Set<EventType> types;
   private final EventManager eventManager;
   
   /**
    * Create an instance of this listener by providing its seagle manager
    * dependency.
    * <p>
    * 
    * @param seagleManager
    *           must not be null.
    */
   protected AbstractSeagleListener(SeagleManager seagleManager, EventManager eventManager) {
      super(seagleManager);
      ArgsCheck.notNull("eventManager", eventManager);
      this.eventManager = eventManager;
      types = getTypes();
   }

   /**
    * Call this to register all event types to listen to. Event types are
    * defined in {@link #getTypes()} method. If this method returns a null set
    * of types this listener will listen to all types of events.
    */
   protected void register() {
      EventManager evm = getEventManager();
      if (types == null) {
         evm.addListener(this);
      } else if (types.isEmpty()) {
         throw new RuntimeException("no event types are defined to listen to");
      } else {
         for (EventType type : types) {
            evm.addListener(type, this);
         }
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void respondToEvent(Event event) {
      EventType type = event.getType();
      if (types == null) {
         acceptEvent(type, event.getInfo());
      } else if (types.contains(type)) {
         acceptEvent(type, event.getInfo());
      }
   }

   /**
    * Will be called each time an event of the defined type is received.
    * <p>
    * The defined event types should be returned by {@link #getTypes()}.
    * 
    * @param type
    *           the type of the event received, which is one of the types
    *           specified by events the subtype provided.
    * @param info
    *           the info of the event to process.
    */
   protected abstract void acceptEvent(EventType type, EventInfo info);

   /**
    * Return the event types this listener should listen to.
    * <p>
    * Override this method, only if you need to listen to certain event types
    * and not all of them. This method should never return an empty set. The
    * event types returned by this method should be defined before
    * {@link #register()} is called otherwise the listener will receive all
    * events.
    * 
    * @return the event types this listener should listen to. Must not be empty.
    */
   protected Set<EventType> getTypes() {
      return null;
   }

   /**
    * @return the event manager which is looked up by seagle manager.
    */
   protected EventManager getEventManager() {
      return eventManager;
   }
}
