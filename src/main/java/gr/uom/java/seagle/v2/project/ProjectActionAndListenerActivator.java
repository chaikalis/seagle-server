package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.ActionType;
import gr.uom.java.seagle.v2.db.persistence.controllers.ActionTypeFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.se.util.manager.annotations.Activator;
import gr.uom.se.util.manager.annotations.Init;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.List;

/**
 *
 * @author Elvis Ligu
 */
@Activator
public class ProjectActionAndListenerActivator {

   private final SeagleManager seagleManager;
   private final ActionTypeFacade actionTypeService;
   private final EventManager eventManager;

   @ProvideModule
   public ProjectActionAndListenerActivator(
         @Property(name = NULLVal.NO_PROP) ActionTypeFacade actionTypeService,
         @Property(name = NULLVal.NO_PROP) SeagleManager seagleManager,
         @Property(name = NULLVal.NO_PROP) EventManager eventManager) {

      ArgsCheck.notNull("seagleManager", seagleManager);
      ArgsCheck.notNull("actionTypeService", actionTypeService);
      ArgsCheck.notNull("eventManager", eventManager);
      this.actionTypeService = actionTypeService;
      this.seagleManager = seagleManager;
      this.eventManager = eventManager;
   }

   @Init
   public void init() {
      activateProjectActions();
      activateProjectEventListeners();
      activateVersionUpdaterListener();
   }

   private void activateProjectActions() {

      for (DBProjectActionType actionType : DBProjectActionType.values()) {
         // Query the action with the given mnemonic
         List<ActionType> list = actionTypeService.findByMnemonic(actionType
               .name());
         // If the action doesn't exist create it in db
         if (list.isEmpty()) {
            ActionType at = new ActionType();
            at.setMnemonic(actionType.name());
            at.setDescription(actionType.description());
            actionTypeService.create(at);
         }
      }
   }

   private void activateProjectEventListeners() {
      // Set the project updater to listen for events of project
      // manager
      DBProjectUpdater projectUpdater = new DBProjectUpdater(seagleManager);
      eventManager.addListener(ProjectEventType.CLONE, projectUpdater);
      eventManager.addListener(ProjectEventType.UPDATE, projectUpdater);
      eventManager.addListener(ProjectEventType.REMOVE, projectUpdater);
      // Set the project action updater to listen for events of
      // project updater
      DBProjectActionUpdater actionUpdater = new DBProjectActionUpdater(
            seagleManager);
      eventManager.addListener(ProjectEventType.DB_INSERTED, actionUpdater);
      eventManager.addListener(ProjectEventType.DB_UPDATED, actionUpdater);
      // Dont need to add the action updater to remove events because
      // it can't create a remove action for a project that has been removed
      // from db.
   }

   private void activateVersionUpdaterListener() {
      VersionCreatorListener listener = new VersionCreatorListener(
            seagleManager, eventManager);
      listener.register();
   }
}
