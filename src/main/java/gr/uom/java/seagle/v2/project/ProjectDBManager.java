package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.ProjectTimeline;

import java.util.Date;
import java.util.List;

/**
 * A manager that handles operations related to project DB entities.
 * <p>
 * Use this manager to gain access into project DB entities and their related
 * services.
 * 
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
public interface ProjectDBManager {

   public abstract List<ProjectTimeline> getTimelinesByProjectURLAndType(
         String url, String actionType);

   public abstract List<ProjectTimeline> getTimelinesByProjectNameAndType(
         String name, String actionType);

   public abstract List<ProjectTimeline> getTimelinesByProjectURL(String url);

   public abstract List<ProjectTimeline> getTimelinesByProjectName(String name);

   public abstract List<ProjectTimeline> getTimelinesByType(String type);

   public abstract List<ProjectTimeline> getTimelines(Date date);

   public abstract List<ProjectTimeline> getTimelines(Date start, Date end);

   public abstract List<ProjectTimeline> getTimelinesBefore(Date date);

   public abstract List<ProjectTimeline> getTimelinesAfter(Date date);

   public abstract void createTimeline(Project project, String actionType,
         Date date);

   public abstract void createTimelineForProjectURL(String url,
         String actionType, Date date);

   public abstract void createTimelineForProjectName(String name,
         String actionType, Date date);

   public abstract ProjectTimeline getLastTimeline(String type);

   public abstract ProjectTimeline getLastTimelineByProjectURL(String url,
         String type);

   public abstract ProjectTimeline getLastTimelineByProjectName(String name,
         String type);

   public abstract void create(Project entity);

   public abstract void edit(Project entity);

   public abstract Project findByName(String name);

   public abstract void remove(Project entity);

   public abstract Project findByUrl(String purl);

   public abstract Project findById(Object id);

   public abstract List<Project> getAll();

   public abstract List<Project> findCreatedAfter(Date date);

   public abstract List<Project> findCreatedBefore(Date date);

   public abstract int count();

   public abstract List<Project> findUpdatedAfter(Date date);

   public abstract List<Project> findUpdatedBefore(Date date);

   public abstract boolean contains(Project entity);

   public abstract List<Project> findInsertedAfter(Date date);

   public abstract List<Project> findInsertedBefore(Date date);

   public abstract List<Project> findCreatedWithin(Date start, Date end);

   public abstract List<Project> findUpdatedWithin(Date start, Date end);

   public abstract List<Project> findInsertedWithin(Date start, Date end);
   
   public abstract void flushChanges();

}