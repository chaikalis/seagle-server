/**
 * 
 */
package gr.uom.java.seagle.v2.metric;

import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.MetricCategory;
import gr.uom.java.seagle.v2.db.persistence.ProgrammingLanguage;
import gr.uom.java.seagle.v2.db.persistence.controllers.MetricCategoryFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProgrammingLanguageFacade;
import java.util.List;
import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
public class LanguageMetricCategoryActivatorTest extends AbstractSeagleTest {
   
   @EJB
   SeagleManager seagleManager;
   
   @EJB
   ProgrammingLanguageFacade languageFacade;
   
   @EJB
   MetricCategoryFacade categoryFacade;

   @Deployment
   public static JavaArchive deploy() {
      return AbstractSeagleTest.deployment();
   }
   
   @Test
   public void testActivationOfProgrammingLanguages() {
      for(Language.Enum pl : Language.Enum.values()) {
         List<ProgrammingLanguage> results = languageFacade.findByName(pl.name());
         assertEquals(1, results.size());
         ProgrammingLanguage language = results.get(0);
         assertNotNull(language);
         assertEquals(pl.name(), language.getName());
      }
   }
   
   @Test
   public void testActivationOfMetricCategories() {
      for(Category.Enum cat : Category.Enum.values()) {
         List<MetricCategory> results = categoryFacade.findByCategory(cat.name());
         assertEquals(1, results.size());
         MetricCategory category = results.get(0);
         assertNotNull(category);
         assertEquals(cat.name(), category.getCategory());
         assertEquals(cat.description(), category.getDescription());
      }
   } 
}
