package gr.uom.java.seagle.v2.analysis.graph;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.se.util.validation.ArgsCheck;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Theodore Chaikalis
 * @param <N>
 * @param <E>
 */
public abstract class SoftwareGraph<N extends AbstractNode, E extends AbstractEdge> extends DirectedSparseGraph<N, E> {

    private Version version;
    private SoftwareProject softwareProject;
    
    private Map<String, Number> metrics;
    
    public SoftwareGraph(){
          metrics = new HashMap<>();
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public N getNode(String name) {
        ArgsCheck.notEmpty("name", name);
        for (N node : this.vertices.keySet()) {
            if (node.getName().equals(name)) {
                return node;
            }
        }
        return null;
    }

    public Collection<N> getVertices() {
        return super.getVertices();
    }

    public SoftwareProject getJavaProject() {
        return softwareProject;
    }

    public void setSoftwareProject(SoftwareProject project) {
        this.softwareProject = project;
    }

    public abstract void accept(GraphVisitor graphVisitor);

}
