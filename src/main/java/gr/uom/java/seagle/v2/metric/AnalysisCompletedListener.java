package gr.uom.java.seagle.v2.metric;

import gr.uom.java.seagle.v2.AbstractSeagleListener;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisCompletedNotificationMailer;
import gr.uom.java.seagle.v2.analysis.AnalysisEventType;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.analysis.smellDetection.events.SmellDetectionManager;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.project.DBProjectActionType;
import gr.uom.java.seagle.v2.project.ProjectDBManager;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventType;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;

/**
 *
 * @author Elvis Ligu
 */
public class AnalysisCompletedListener extends AbstractSeagleListener {

    private EventManager eventManager;
    
    public AnalysisCompletedListener(SeagleManager seagleManager,
            EventManager eventManager) {
        super(seagleManager, eventManager);
        this.eventManager = eventManager;
    }

    @Override
    protected void register() {
        super.register();
    }

    @Override
    protected void acceptEvent(EventType type, EventInfo info) {
        if (!(type.equals(AnalysisEventType.ANALYSIS_COMPLETED))) {
            throw new RuntimeException("type " + type + " was not supposed to be received");
        }
       
        AnalysisRequestInfo ainfo = (AnalysisRequestInfo) info.getDescription();
        
        createAnalyzedAction(ainfo, new Date());
        sendNotificationEmail(ainfo);
    }

    private void sendNotificationEmail(AnalysisRequestInfo analysisInfo) {
        try {
            String email = analysisInfo.getEmail();
            String remoteProjectUrl = analysisInfo.getRemoteProjectUrl();
            ProjectManager projectManager = seagleManager.resolveComponent(ProjectManager.class);
            Project project = projectManager.findByUrl(remoteProjectUrl);
            AnalysisCompletedNotificationMailer.sendNotificationEmailUsingGmail(seagleManager, email, project);
            java.util.logging.Logger.getLogger(AnalysisCompletedListener.class.getName()).info("Notification Email sent");
        } catch (MessagingException ex) {
            Logger.getLogger(AnalysisCompletedListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected Set<EventType> getTypes() {
        Set<EventType> types = new HashSet<>();
        types.add(AnalysisEventType.ANALYSIS_COMPLETED);
        return types;
    }

    private void createAnalyzedAction(AnalysisRequestInfo info, Date startDate) {
        createAction(info, startDate, DBProjectActionType.PROJECT_ANALYSED);
    }

    private void createAction(AnalysisRequestInfo info, Date startDate,
            DBProjectActionType ptype) {
        ProjectDBManager dbManager = resolveComponentOrManager(ProjectDBManager.class);
        dbManager.createTimelineForProjectURL(info.getRemoteProjectUrl(), ptype.name(),
                startDate);
    }
}
