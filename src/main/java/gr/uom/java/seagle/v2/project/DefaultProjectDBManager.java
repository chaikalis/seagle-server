/**
 *
 */
package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.ActionType;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.ProjectInfo;
import gr.uom.java.seagle.v2.db.persistence.ProjectTimeline;
import gr.uom.java.seagle.v2.db.persistence.controllers.ActionTypeFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectInfoFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectTimelineFacade;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.Date;
import java.util.List;

/**
 * A default implementation of {@link ProjectDBManager}.
 * <p>
 * This class is based on managers API and modules API thus it should be
 * included in configuration files info to register this manager class when
 * seagle is starting and to load this object when it is requested.
 *
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
public class DefaultProjectDBManager implements ProjectDBManager {

    private final SeagleManager seagleManager;

    @ProvideModule
    public DefaultProjectDBManager(
            @Property(name = NULLVal.NO_PROP) SeagleManager seagleManager) {
        ArgsCheck.notNull("seagleManager", seagleManager);
        this.seagleManager = seagleManager;
    }

    private ProjectFacade resolveProjectFacade() {
        return resolveComponent(ProjectFacade.class);
    }

    private ProjectTimelineFacade resolveProjectTimeline() {
        return resolveComponent(ProjectTimelineFacade.class);
    }

    private ActionTypeFacade resolveActionTypeFacade() {
        return resolveComponent(ActionTypeFacade.class);
    }

    private <T> T resolveComponent(Class<T> type) {
        T component = seagleManager.resolveComponent(type);
        if (component == null) {
            throw new RuntimeException("unable to resolve " + type
                    + " from sealge manager");
        }
        return component;
    }

    @Override
    public List<ProjectTimeline> getTimelinesByProjectURLAndType(String url,
            String actionType) {
        return resolveProjectTimeline().findByProjectURLAndType(url, actionType);
    }

    @Override
    public List<ProjectTimeline> getTimelinesByProjectNameAndType(String name,
            String actionType) {
        return resolveProjectTimeline()
                .findByProjectNameAndType(name, actionType);
    }

    @Override
    public List<ProjectTimeline> getTimelinesByProjectURL(String url) {
        return resolveProjectTimeline().findByProjectURL(url);
    }

    @Override
    public List<ProjectTimeline> getTimelinesByProjectName(String name) {
        return resolveProjectTimeline().findByProjectName(name);
    }

    @Override
    public List<ProjectTimeline> getTimelinesByType(String type) {
        return resolveProjectTimeline().findByType(type);
    }

    @Override
    public List<ProjectTimeline> getTimelines(Date date) {
        return resolveProjectTimeline().findByDate(date);
    }

    @Override
    public List<ProjectTimeline> getTimelines(Date start, Date end) {
        ArgsCheck.isTrue("start < end", start.before(end));
        return resolveProjectTimeline().findWithin(start, end);
    }

    @Override
    public List<ProjectTimeline> getTimelinesBefore(Date date) {
        return resolveProjectTimeline().findBefore(date);
    }

    @Override
    public List<ProjectTimeline> getTimelinesAfter(Date date) {
        return resolveProjectTimeline().findAfter(date);
    }

    @Override
    public void createTimeline(Project project, String actionType, Date date) {
        ArgsCheck.notNull("project", project);
        ArgsCheck.notNull("date", date);
        ArgsCheck.notNull("actionType", actionType);

        ActionTypeFacade acf = resolveActionTypeFacade();
        List<ActionType> list = acf.findByMnemonic(actionType);
        if (list.isEmpty()) {
            throw new IllegalArgumentException("action type is unknown "
                    + actionType);
        }
        ActionType type = list.get(0);

        // Create the timeline
        ProjectTimeline timeline = new ProjectTimeline();
        timeline.setTimestamp(date);
        timeline.setProject(project);
        timeline.setActionType(type);
      // Add timline to project, to maintain bidirectional
        // JPA relation
        project.addTimeLine(timeline);
        // Add this timeline to type too
        type.getProjectTimelines().add(timeline);

        // Edit the action type first
        acf.edit(type);
        // Edit the project
        resolveProjectFacade().edit(project);
        // Now create the timeline
        resolveProjectTimeline().create(timeline);
    }

    @Override
    public void createTimelineForProjectURL(String url, String actionType,
            Date date) {
        ArgsCheck.notEmpty("url", url);
        List<Project> list = resolveProjectFacade().findByUrl(url);
        if (list.isEmpty()) {
            throw new IllegalArgumentException("project with url " + url
                    + " is missing");
        }
        createTimeline(list.get(0), actionType, date);
    }

    @Override
    public void createTimelineForProjectName(String name, String actionType,
            Date date) {
        ArgsCheck.notEmpty("name", name);
        List<Project> list = resolveProjectFacade().findByName(name);
        if (list.isEmpty()) {
            throw new IllegalArgumentException("project with name " + name
                    + " is missing");
        }
        createTimeline(list.get(0), actionType, date);
    }

    @Override
    public ProjectTimeline getLastTimeline(String type) {
        return resolveProjectTimeline().findLast(type);
    }

    @Override
    public ProjectTimeline getLastTimelineByProjectURL(String url, String type) {
        return resolveProjectTimeline().findLastByProjectUrl(url, type);
    }

    @Override
    public ProjectTimeline getLastTimelineByProjectName(String name, String type) {
        return resolveProjectTimeline().findLastByProjectName(name, type);
    }

    @Override
    public void create(Project entity) {
        ProjectFacade facade = resolveProjectFacade();
        facade.create(entity);
        facade.flushChanges();
    }

    @Override
    public void edit(Project entity) {
        resolveProjectFacade().edit(entity);
    }

    @Override
    public Project findByName(String name) {

        List<Project> list = resolveProjectFacade().findByName(name);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public void remove(Project entity) {
        ProjectFacade facade = resolveProjectFacade();
        facade.remove(entity);
        facade.flushChanges();
    }

    @Override
    public Project findByUrl(String purl) {
        List<Project> list = resolveProjectFacade().findByUrl(purl);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public Project findById(Object id) {
        return resolveProjectFacade().findById(id);
    }

    @Override
    public List<Project> getAll() {
        return resolveProjectFacade().findAll();
    }

    @Override
    public List<Project> findCreatedAfter(Date date) {
        return resolveProjectFacade().findCreatedAfter(date);
    }

    @Override
    public List<Project> findCreatedBefore(Date date) {
        return resolveProjectFacade().findCreatedBefore(date);
    }

    @Override
    public int count() {
        return resolveProjectFacade().count();
    }

    @Override
    public List<Project> findUpdatedAfter(Date date) {
        return resolveProjectFacade().findUpdatedAfter(date);
    }

    @Override
    public List<Project> findUpdatedBefore(Date date) {
        return resolveProjectFacade().findUpdatedBefore(date);
    }

    @Override
    public boolean contains(Project entity) {
        return resolveProjectFacade().contains(entity);
    }

    @Override
    public List<Project> findInsertedAfter(Date date) {
        return resolveProjectFacade().findInsertedAfter(date);
    }

    @Override
    public List<Project> findInsertedBefore(Date date) {
        return resolveProjectFacade().findInsertedBefore(date);
    }

    @Override
    public List<Project> findCreatedWithin(Date start, Date end) {
        return resolveProjectFacade().findCreatedWithin(start, end);
    }

    @Override
    public List<Project> findUpdatedWithin(Date start, Date end) {
        return resolveProjectFacade().findUpdatedWithin(start, end);
    }

    @Override
    public List<Project> findInsertedWithin(Date start, Date end) {
        return resolveProjectFacade().findInsertedWithin(start, end);
    }
    
    @Override
    public void flushChanges(){
        resolveProjectFacade().flushChanges();
    }
}
