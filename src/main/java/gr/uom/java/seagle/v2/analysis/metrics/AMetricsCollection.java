package gr.uom.java.seagle.v2.analysis.metrics;

import gr.uom.java.seagle.v2.analysis.metrics.graph.Nodes;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.metrics.graph.BetweennessCentralityCalc;
import gr.uom.java.seagle.v2.analysis.metrics.graph.ClusteringCoefficient;
import gr.uom.java.seagle.v2.analysis.metrics.graph.Density;
import gr.uom.java.seagle.v2.analysis.metrics.graph.Diameter;
import gr.uom.java.seagle.v2.analysis.metrics.graph.Edges;
import gr.uom.java.seagle.v2.analysis.metrics.graph.evolutionary.EdgesToExistingNodes;
import gr.uom.java.seagle.v2.analysis.metrics.graph.evolutionary.ExistingNodesPerVersion;
import gr.uom.java.seagle.v2.analysis.metrics.graph.InDegree;
import gr.uom.java.seagle.v2.analysis.metrics.graph.evolutionary.NewNodesPerVersion;
import gr.uom.java.seagle.v2.analysis.metrics.graph.OutDegree;
import gr.uom.java.seagle.v2.analysis.metrics.graph.evolutionary.NodeAge;
import gr.uom.java.seagle.v2.analysis.metrics.graph.PageRankCalc;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.ATFD;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.CBO;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.FDP;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.LAA;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.TCC;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.LCOM;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.LOC;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.NOAM;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.NOF;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.NOM;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.NOPA;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.WMC;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.WOC;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Theodore Chaikalis
 */
public class AMetricsCollection {

    private static final Map<String, AbstractAnalysisMetric> systemRegisteredGraphMetrics = new LinkedHashMap<>();
    private static final Map<String, AbstractAnalysisMetric> systemRegisteredSourceCodeMetrics = new LinkedHashMap<>();
    private static final Map<String, AbstractAnalysisMetric> systemRegisteredMetrics = new LinkedHashMap<>();

    public static void createSystemMetrics(SeagleManager seagleManager) { 
        createGraphMetrics(seagleManager);
        createSourceCodeMetrics(seagleManager);
        systemRegisteredMetrics.putAll(systemRegisteredGraphMetrics);
        systemRegisteredMetrics.putAll(systemRegisteredSourceCodeMetrics);
    }

    private static void createSourceCodeMetrics(SeagleManager seagleManager) {
       systemRegisteredSourceCodeMetrics.put(ATFD.MNEMONIC, new ATFD(seagleManager));
        systemRegisteredSourceCodeMetrics.put(TCC.MNEMONIC, new TCC(seagleManager));
        systemRegisteredSourceCodeMetrics.put(WMC.MNEMONIC, new WMC(seagleManager));
        systemRegisteredSourceCodeMetrics.put(CBO.MNEMONIC, new CBO(seagleManager));
        systemRegisteredSourceCodeMetrics.put(LCOM.MNEMONIC, new LCOM(seagleManager));
        systemRegisteredSourceCodeMetrics.put(LOC.MNEMONIC, new LOC(seagleManager));
        systemRegisteredSourceCodeMetrics.put(NOAM.MNEMONIC, new NOAM(seagleManager));
        systemRegisteredSourceCodeMetrics.put(NOM.MNEMONIC, new NOM(seagleManager));
        systemRegisteredSourceCodeMetrics.put(NOF.MNEMONIC, new NOF(seagleManager));
        systemRegisteredSourceCodeMetrics.put(NOPA.MNEMONIC, new NOPA(seagleManager));

        systemRegisteredSourceCodeMetrics.put(WOC.MNEMONIC, new WOC(seagleManager));
        systemRegisteredSourceCodeMetrics.put(LAA.MNEMONIC, new LAA(seagleManager));
        systemRegisteredSourceCodeMetrics.put(FDP.MNEMONIC, new FDP(seagleManager));
    }

    private static void createGraphMetrics(SeagleManager seagleManager) {

        systemRegisteredGraphMetrics.put(Nodes.MNEMONIC, new Nodes(seagleManager));
        systemRegisteredGraphMetrics.put(Edges.MNEMONIC, new Edges(seagleManager));
        systemRegisteredGraphMetrics.put(BetweennessCentralityCalc.MNEMONIC, new BetweennessCentralityCalc(seagleManager));
        systemRegisteredGraphMetrics.put(Density.MNEMONIC, new Density(seagleManager));
        systemRegisteredGraphMetrics.put(Diameter.MNEMONIC, new Diameter(seagleManager));
        systemRegisteredGraphMetrics.put(ClusteringCoefficient.MNEMONIC, new ClusteringCoefficient(seagleManager));

//        systemRegisteredGraphMetrics.put(PageRankCalc.MNEMONIC, new PageRankCalc(seagleManager));
//        systemRegisteredGraphMetrics.put(InDegree.MNEMONIC, new InDegree(seagleManager));
//        systemRegisteredGraphMetrics.put(OutDegree.MNEMONIC, new OutDegree(seagleManager));
        
//        systemRegisteredGraphMetrics.put(NodeAge.MNEMONIC, new NodeAge(seagleManager));
//        systemRegisteredGraphMetrics.put(AverageInDegree.MNEMONIC, new AverageInDegree(seagleManager));
//        systemRegisteredGraphMetrics.put(ExistingNodesPerVersion.MNEMONIC, new ExistingNodesPerVersion(seagleManager));
//        systemRegisteredGraphMetrics.put(DeletedNodesPerVersion.MNEMONIC, new DeletedNodesPerVersion(seagleManager));
//        systemRegisteredGraphMetrics.put(NewNodesPerVersion.MNEMONIC, new NewNodesPerVersion(seagleManager));
//        systemRegisteredGraphMetrics.put(EdgesToExistingNodes.MNEMONIC, new EdgesToExistingNodes(seagleManager));
    }

    public static Set<String> getMetricMnemonics() {
        return systemRegisteredMetrics.keySet();
    }

    public static AbstractAnalysisMetric getMetric(String mnemonic) {
        return systemRegisteredMetrics.get(mnemonic);
    }

    /*
     Age Updater needs special handling because it must be the first updater to run. 
     So it must be provided separately from the other metrics
     */
    public static AbstractAnalysisMetric getAgeUpdater() {
        return systemRegisteredGraphMetrics.get(NodeAge.MNEMONIC);
    }

    public static Collection<AbstractAnalysisMetric> getCreatedGraphMetrics() {
        return systemRegisteredGraphMetrics.values();
    }

    public static Collection<AbstractAnalysisMetric> getCreatedSourceCodeMetrics() {
        return systemRegisteredSourceCodeMetrics.values();
    }

    /*
     NODES("Number of Nodes", "Number of nodes of a graph that represent a software project at a specific version") {

     EDGES("Number of Edges", "Number of edges of a graph that represent a software project at a specific version"),
    
     DENSITY("Density", "Density of a graph that represent a software project at a specific version"),
    
     ALPHA("Alpha, the power factor", "Alpha value of a graph that represent a software project at a specific version"),

     SYSTEM_CLUSTERING_COEFFICIENT("System Clustering Coefficient", "Clustering coefficient for the whole graph on average"),
    
     NODE_CLUSTERING_COEFFICIENT("Node Clustering Coefficient",  "Clustering coefficient for each node"),
    
     SYSTEM_PAGERANK("System Page Rank", "PageRank for the whole graph on average"),
    
     NODE_PAGERANK("Node Page Rank",  "PageRank for each node"),
    
     SYSTEM_BETWEENNESS_CENTRALITY("System Betweenness Centrality", "Betweenness Centrality for the whole graph on average"),
    
     NODE_BETWEENNESS_CENTRALITY("Node Betweenness Centrality",  "Betweenness Centrality for each node")
     */
}
