/**
 * 
 */
package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.Node;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
@Stateless
public class VersionFacade extends AbstractFacade<Version> {

   /**
    * Injected entity manager for the persistence context named {@code seaglePU}
    * .
    * <p>
    */
   @PersistenceContext(unitName = "seaglePU")
   private EntityManager em;

   @Inject
   private NodeFacade nodeFacade;
   
   @Override
   protected EntityManager getEntityManager() {
      return em;
   }

   /**
    * Create an instance of this service by specifying the entity type of class
    * {@link Version}.
    */
   public VersionFacade() {
      super(Version.class);
   }

   public List<Version> findByCommitID(Project p, String cid) {
      List<Version> list = JPAUtils.namedQueryEntityTwoParam(em, Version.class,
            "Version.findByCommitIDandProject", "project", p, "cid", cid);
      return list;
   }
   
   public List<Version> findByCommitID(String cid) {
      List<Version> list = JPAUtils.namedQueryEntityOneParam(em, Version.class,
            "Version.findByCommitID", "commitID", cid);
      return list;
   }

   /**
    * Find all versions of the given project.
    * <p>
    * 
    * @param project
    *           the project entity which has the versions to be find
    * @return all versions of the given project.
    */
   public List<Version> findByProject(Project project) {
      ArgsCheck.notNull("project", project);
      String query = "Version.findByProject";
      String param = "project";
      Object val = project;
      return JPAUtils.namedQueryEntityOneParam(em, Version.class, query, param,
            val);
   }
   
   /**
    * Find all versions of the given project with the given url.
    * <p>
    * 
    * @param project
    *           the project entity which has the versions to be find
    * @return all versions of the given project.
    */
   public List<Version> findByProjectURL(String url) {
      ArgsCheck.notNull("project", url);
      String query = "Version.findByProjectURL";
      String param = "url";
      Object val = url;
      return JPAUtils.namedQueryEntityOneParam(em, Version.class, query, param,
            val);
   }
}
